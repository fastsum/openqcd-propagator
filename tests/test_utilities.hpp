/*
 * Created: 19-09-2018
 * Copyright (c) <2018> <Jonas R. Glesaaen (jonas@glesaaen.com)>
 */

#ifndef TEST_UTILITIES_HPP
#define TEST_UTILITIES_HPP

#include <array>
#include <cmath>
#include <deque>
#include <mpi.h>
#include <numeric>

inline int get_mpi_rank()
{
  int mpi_rank = 0;
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);
  return mpi_rank;
}

inline int get_mpi_size()
{
  int mpi_size = 0;
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);
  return mpi_size;
}

inline bool mpi_is_finalized()
{
  int is_finalized;
  MPI_Finalized(&is_finalized);
  return (is_finalized != 0);
}

inline void mpi_conditional_finalize()
{
  if (!mpi_is_finalized()) {
    MPI_Finalize();
  }
}

inline std::array<int, 4> automatic_processor_grid()
{
  int mpi_size = get_mpi_size();

  if (mpi_size == 1) {
    return {1, 1, 1, 1};
  }

  // Find the prime factors of the MPI size
  std::deque<int> factors;
  int current = 2;
  int upper = std::floor(std::sqrt(mpi_size));

  while (current <= upper) {
    if (mpi_size % current == 0) {
      factors.push_back(current);
      mpi_size /= current;
      upper = std::floor(std::sqrt(mpi_size));
    } else {
      ++current;
    }
  }

  if (mpi_size > 1) {
    factors.push_back(mpi_size);
  }

  std::array<int, 4> grid{1, 1, 1, 1};

  // Add a factor of 2 to every possible "bin"
  for (auto i = 3; i >= 0; --i) {
    if (factors.front() == 2) {
      grid[i] *= 2;
      factors.pop_front();
    }
  }

  // Check that we have at least 1 factor of 2
  if (std::accumulate(grid.begin(), grid.end(), 1,
                      [](int a, int b) { return a * b; }) %
          2 !=
      0) {
    throw std::invalid_argument{
        "Cannot run openqcd with an odd number of threads"};
  }

  auto next_bin = 3;

  // Add factors in the bins, trying to make Nz the largest
  while (!factors.empty()) {

    grid[next_bin] *= factors.back();
    factors.pop_back();

    do {
      next_bin = (next_bin + 3) % 4;
    } while (grid[next_bin] % 2 != 0);
  }

  return grid;
}

#endif /* TEST_UTILITIES_HPP */
