/*
 * Created: 30-08-2018
 * Copyright (c) <2018> <Jonas R. Glesaaen (jonas@glesaaen.com)>
 */

#ifndef OPENQCD_ENVIRONMENT_HPP
#define OPENQCD_ENVIRONMENT_HPP

#include "test_utilities.hpp"
#include <gtest-mpi-listener.hpp>
#include <gtest/gtest.h>
#include <mpi.h>

extern "C" {
#include "propagator.h"

#include "openqcd/c_headers/flags.h"
#include "openqcd/c_headers/global.h"
#include "openqcd/c_headers/lattice.h"
}

namespace fastsum {
namespace openqcd {

class OpenQCD_Environment : public ::testing::Environment
{
public:
  OpenQCD_Environment(std::array<int, 4> processor_grid)
      : processor_grid_{processor_grid} {};

  void SetUp() override
  {
    int nproc[] = {processor_grid_[0], processor_grid_[1], processor_grid_[2],
                   processor_grid_[3]};
    int local_size[] = {8, 8, 8, 8};
    double theta[] = {0.0, 0.0, 0.0};

    if (get_mpi_rank() == 0) {
      gtest_mpi_listener::internal::ColoredPrintf(
          gtest_mpi_listener::internal::COLOR_YELLOW, "[   INFO   ] ");
      printf("Processor grid: [%3d,%3d,%3d,%3d]\n", nproc[0], nproc[1],
             nproc[2], nproc[3]);
      gtest_mpi_listener::internal::ColoredPrintf(
          gtest_mpi_listener::internal::COLOR_YELLOW, "[   INFO   ] ");
      printf("Local lattice:  [%3d,%3d,%3d,%3d]\n", local_size[0],
             local_size[1], local_size[2], local_size[3]);
      gtest_mpi_listener::internal::ColoredPrintf(
          gtest_mpi_listener::internal::COLOR_YELLOW, "[   INFO   ] ");
      printf("Global lattice: [%3d,%3d,%3d,%3d]\n", nproc[0] * local_size[0],
             nproc[1] * local_size[1], nproc[2] * local_size[2],
             nproc[3] * local_size[3]);
    }

    openqcd__set_lattice_sizes(nproc, local_size, nproc);
    openqcd_flags__set_bc_parms(3, 0.0, 0.0, 0.0, 0.0, nullptr, nullptr, theta);
    openqcd_flags__set_no_ani_parms();
    openqcd_propagator__set_dirac_operator_parms(0.0, 0.0, 0, nullptr, 0);
    openqcd_lattice__geometry();

    openqcd_utils__alloc_wsd(4);
  }

  virtual ~OpenQCD_Environment(){};

private:
  std::array<int, 4> processor_grid_;
};

} // namespace openqcd
} // namespace fastsum

#endif /* OPENQCD_ENVIRONMENT_HPP */
