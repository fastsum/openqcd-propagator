/*
 * Created: 03-06-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gtest/gtest.h>

#include "lib/sources.c"
#include "openqcd_test_fixture.hpp"
#include "test_utilities.hpp"

extern "C" {

#include "propagator.h"

#include "openqcd/c_headers/linalg.h"
#include "openqcd/c_headers/sflds.h"
#include "openqcd/c_headers/su3fcts.h"
#include "openqcd/c_headers/uflds.h"
}

using namespace fastsum::openqcd;

TEST_F(Sources, Test_Point_Source_Origin)
{
  int origin[4] = {0, 0, 0, 0};
  auto propagator = propagator_field();
  set_source_parms(0, POINT_SOURCE, origin, 0, 0, 0, 0.0);

  set_sd2zero(12 * VOLUME, *propagator);

  auto mpi_rank = get_mpi_rank();
  auto expected_value = 0.0;

  if (mpi_rank == 0) {
    expected_value = 1.0;
  }

  generate_source(0, 0, propagator[0]);
  EXPECT_DOUBLE_EQ(expected_value, propagator[0][0].c1.c1.re);
  EXPECT_DOUBLE_EQ(1.0, norm_square_dble(12 * VOLUME, 1, *propagator));

  generate_source(0, 1, propagator[0]);
  EXPECT_DOUBLE_EQ(expected_value, propagator[0][0].c1.c2.re);
  EXPECT_DOUBLE_EQ(1.0, norm_square_dble(12 * VOLUME, 1, *propagator));

  generate_source(0, 2, propagator[0]);
  EXPECT_DOUBLE_EQ(expected_value, propagator[0][0].c1.c3.re);
  EXPECT_DOUBLE_EQ(1.0, norm_square_dble(12 * VOLUME, 1, *propagator));

  generate_source(0, 3, propagator[0]);
  EXPECT_DOUBLE_EQ(expected_value, propagator[0][0].c2.c1.re);
  EXPECT_DOUBLE_EQ(1.0, norm_square_dble(12 * VOLUME, 1, *propagator));

  generate_source(0, 11, propagator[0]);
  EXPECT_DOUBLE_EQ(expected_value, propagator[0][0].c4.c3.re);
  EXPECT_DOUBLE_EQ(1.0, norm_square_dble(12 * VOLUME, 1, *propagator));
}

TEST_F(Sources, Test_Point_Source_Mid)
{
  int mid[4] = {L0 / 2, L1 / 2, L2 / 2, L3 / 2};
  auto propagator = propagator_field();
  set_source_parms(0, POINT_SOURCE, mid, 0, 0, 0, 0.0);

  set_sd2zero(12 * VOLUME, *propagator);

  auto pt = ipt[mid[3] + L3 * (mid[2] + L2 * (mid[1] + L3 * mid[0]))];

  auto mpi_rank = get_mpi_rank();
  auto expected_value = 0.0;

  if (mpi_rank == 0) {
    expected_value = 1.0;
  }

  generate_source(0, 0, propagator[0]);
  EXPECT_DOUBLE_EQ(expected_value, propagator[0][pt].c1.c1.re);
  EXPECT_DOUBLE_EQ(1.0, norm_square_dble(12 * VOLUME, 1, *propagator));

  generate_source(0, 1, propagator[0]);
  EXPECT_DOUBLE_EQ(expected_value, propagator[0][pt].c1.c2.re);
  EXPECT_DOUBLE_EQ(1.0, norm_square_dble(12 * VOLUME, 1, *propagator));

  generate_source(0, 2, propagator[0]);
  EXPECT_DOUBLE_EQ(expected_value, propagator[0][pt].c1.c3.re);
  EXPECT_DOUBLE_EQ(1.0, norm_square_dble(12 * VOLUME, 1, *propagator));

  generate_source(0, 3, propagator[0]);
  EXPECT_DOUBLE_EQ(expected_value, propagator[0][pt].c2.c1.re);
  EXPECT_DOUBLE_EQ(1.0, norm_square_dble(12 * VOLUME, 1, *propagator));

  generate_source(0, 11, propagator[0]);
  EXPECT_DOUBLE_EQ(expected_value, propagator[0][pt].c4.c3.re);
  EXPECT_DOUBLE_EQ(1.0, norm_square_dble(12 * VOLUME, 1, *propagator));
}

TEST_F(Sources, Test_Smeared_Source_Origin)
{
  int origin[4] = {0, 0, 0, 0};
  auto propagator = propagator_field();
  set_source_parms(0, GAUSSIAN_SMEARED_SOURCE, origin, 0, 0, 5, 0.25);

  auto diag = std::pow(1 - 6 * 0.25 * 0.25 / (4 * 5), 5);
  auto diag_sqr = diag * diag;

  cm3x3_zero(4 * VOLUME, udfld());
  copy_bnd_ud();

  auto mpi_rank = get_mpi_rank();
  auto expected_value = 0.0;

  if (mpi_rank == 0) {
    expected_value = diag;
  }

  set_sd2zero(12 * VOLUME, *propagator);

  generate_source(0, 0, propagator[0]);
  EXPECT_NEAR(expected_value, propagator[0][0].c1.c1.re, 1e-12 * diag);
  EXPECT_NEAR(diag_sqr, norm_square_dble(12 * VOLUME, 1, *propagator),
              1e-12 * diag_sqr);

  generate_source(0, 1, propagator[0]);
  EXPECT_NEAR(expected_value, propagator[0][0].c1.c2.re, 1e-12 * diag);
  EXPECT_NEAR(diag_sqr, norm_square_dble(12 * VOLUME, 1, *propagator),
              1e-12 * diag_sqr);

  generate_source(0, 2, propagator[0]);
  EXPECT_NEAR(expected_value, propagator[0][0].c1.c3.re, 1e-12 * diag);
  EXPECT_NEAR(diag_sqr, norm_square_dble(12 * VOLUME, 1, *propagator),
              1e-12 * diag_sqr);

  generate_source(0, 3, propagator[0]);
  EXPECT_NEAR(expected_value, propagator[0][0].c2.c1.re, 1e-12 * diag);
  EXPECT_NEAR(diag_sqr, norm_square_dble(12 * VOLUME, 1, *propagator),
              1e-12 * diag_sqr);

  generate_source(0, 11, propagator[0]);
  EXPECT_NEAR(expected_value, propagator[0][0].c4.c3.re, 1e-12 * diag);
  EXPECT_NEAR(diag_sqr, norm_square_dble(12 * VOLUME, 1, *propagator),
              1e-12 * diag_sqr);
}

TEST_F(Sources, Test_Smeared_Source_Mid)
{
  int mid[4] = {L0 / 2, L1 / 2, L2 / 2, L3 / 2};
  auto propagator = propagator_field();
  set_source_parms(0, GAUSSIAN_SMEARED_SOURCE, mid, 0, 0, 5, 0.25);

  auto pt = ipt[mid[3] + L3 * (mid[2] + L2 * (mid[1] + L3 * mid[0]))];

  auto diag = std::pow(1 - 6 * 0.25 * 0.25 / (4 * 5), 5);
  auto diag_sqr = diag * diag;

  cm3x3_zero(4 * VOLUME, udfld());
  copy_bnd_ud();

  auto mpi_rank = get_mpi_rank();
  auto expected_value = 0.0;

  if (mpi_rank == 0) {
    expected_value = diag;
  }

  set_sd2zero(12 * VOLUME, *propagator);

  generate_source(0, 0, propagator[0]);
  EXPECT_NEAR(expected_value, propagator[0][pt].c1.c1.re, 1e-12 * diag);
  EXPECT_NEAR(diag_sqr, norm_square_dble(12 * VOLUME, 1, *propagator),
              1e-12 * diag_sqr);

  generate_source(0, 1, propagator[0]);
  EXPECT_NEAR(expected_value, propagator[0][pt].c1.c2.re, 1e-12 * diag);
  EXPECT_NEAR(diag_sqr, norm_square_dble(12 * VOLUME, 1, *propagator),
              1e-12 * diag_sqr);

  generate_source(0, 2, propagator[0]);
  EXPECT_NEAR(expected_value, propagator[0][pt].c1.c3.re, 1e-12 * diag);
  EXPECT_NEAR(diag_sqr, norm_square_dble(12 * VOLUME, 1, *propagator),
              1e-12 * diag_sqr);

  generate_source(0, 3, propagator[0]);
  EXPECT_NEAR(expected_value, propagator[0][pt].c2.c1.re, 1e-12 * diag);
  EXPECT_NEAR(diag_sqr, norm_square_dble(12 * VOLUME, 1, *propagator),
              1e-12 * diag_sqr);

  generate_source(0, 11, propagator[0]);
  EXPECT_NEAR(expected_value, propagator[0][pt].c4.c3.re, 1e-12 * diag);
  EXPECT_NEAR(diag_sqr, norm_square_dble(12 * VOLUME, 1, *propagator),
              1e-12 * diag_sqr);
}
