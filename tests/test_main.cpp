/*
 * Created: 30-08-2018
 * Copyright (c) <2018> <Jonas R. Glesaaen (jonas@glesaaen.com)>
 */

#include <gmock/gmock.h>
#include <gtest-mpi-listener.hpp>
#include <mpi.h>
#include <numeric>

#include "openqcd_environment.hpp"
#include "test_utilities.hpp"

using namespace fastsum::openqcd;

std::array<int, 4> parse_processor_grid(int argc, char **argv);

void report_error_and_usage(std::exception &err);

int main(int argc, char **argv)
{
  MPI_Init(&argc, &argv);
  ::testing::InitGoogleMock(&argc, argv);

  std::array<int, 4> processor_grid;

  try {
    processor_grid = parse_processor_grid(argc, argv);
  } catch (std::invalid_argument &err) {
    report_error_and_usage(err);
    MPI_Finalize();
    return 0;
  }

  ::testing::AddGlobalTestEnvironment(new gtest_mpi_listener::MPIEnvironment);
  ::testing::AddGlobalTestEnvironment(new OpenQCD_Environment{processor_grid});

  // Get the event listener list.
  ::testing::TestEventListeners &listeners =
      ::testing::UnitTest::GetInstance()->listeners();

  // Remove default listener
  delete listeners.Release(listeners.default_result_printer());

  // Adds MPI listener; Google Test owns this pointer
  listeners.Append(new gtest_mpi_listener::PrettyMPIPrinter);

  return RUN_ALL_TESTS();
}

std::array<int, 4> parse_processor_grid(int argc, char **argv)
{
  if (argc < 2) {
    return automatic_processor_grid();
  }

  std::string buffer;
  std::istringstream token_stream{std::string{argv[1]}};

  int token_count = 0;
  std::array<int, 4> processor_grid;

  while (std::getline(token_stream, buffer, ',')) {

    if (token_count >= 4) {
      throw std::invalid_argument{
          "Expected 4 numbers for the processor grid, more specified"};
    }

    try {
      processor_grid[token_count] = std::stoi(buffer);
    } catch (std::invalid_argument &err) {
      throw std::invalid_argument{"Invalid processor grid argument"};
    }

    token_count += 1;
  }

  if (token_count != 4) {
    std::ostringstream oss;
    oss << "Expected 4 numbers for the processor grid, got " << token_count;
    throw std::invalid_argument{oss.str()};
  }

  int mpi_size = 0;
  MPI_Comm_size(MPI_COMM_WORLD, &mpi_size);

  auto arg_mpi_size =
      std::accumulate(processor_grid.begin(), processor_grid.end(), 1,
                      [](int a, int b) { return a * b; });

  if (mpi_size != arg_mpi_size) {
    throw std::invalid_argument{
        "Specified processor grid does not match MPI comm size"};
  }

  return processor_grid;
}

void report_error_and_usage(std::exception &err)
{
  int mpi_rank = 0;
  MPI_Comm_rank(MPI_COMM_WORLD, &mpi_rank);

  if (mpi_rank != 0) {
    return;
  }

  std::cerr << "Error: " << err.what() << "\n\n"
            << "Usage: run_tests [processor_grid]\n\n"
            << " - processor_grid (str) -- Nt,Nx,Ny,Nz     A comma separated "
               "list of the number\n"
            << "                                           of cores in each of "
               "the four space-\n"
            << "                                           time directions.\n";
}
