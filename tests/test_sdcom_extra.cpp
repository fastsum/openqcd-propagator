/*
 * Created: 30-08-2018
 * Copyright (c) <2018> <Jonas R. Glesaaen (jonas@glesaaen.com)>
 */

#include <gtest/gtest.h>

#include "lib/sdcom_extra.c"
#include "test_utilities.hpp"

extern "C" {
#include "propagator.h"

#include "openqcd/c_headers/linalg.h"
#include "openqcd/c_headers/sflds.h"
#include "openqcd/c_headers/su3fcts.h"
#include "openqcd/c_headers/uflds.h"
}

const std::vector<int> face_sizes = {FACE0 / 2, FACE0 / 2, FACE1 / 2,
                                     FACE1 / 2, FACE2 / 2, FACE2 / 2,
                                     FACE3 / 2, FACE3 / 2};

void unzip_weyl(spinor_dble &s, weyl_dble const &w)
{
  static su3_vector_dble v0 = {{0.0, 0.0}, {0.0, 0.0}, {0.0, 0.0}};

  _vector_add(s.c1, w.c1, w.c1);
  _vector_add(s.c2, w.c2, w.c2);
  s.c3 = v0;
  s.c4 = v0;
}

/* Get the vector of offsets on the halo for the 8 faces */
std::vector<int> get_halo_offsets()
{
  static std::vector<int> offsets{};

  if (offsets.empty()) {
    offsets.resize(8);

    offsets[0] = 0;
    for (auto face = 1; face < 8; ++face) {
      offsets[face] = face_sizes[face - 1] + offsets[face - 1];
    }
  }

  return offsets;
}

/* Check that copying the boundary, multiplying by minus-one, then adding back
 * gives zero at the right places */
TEST(Spinor_Comm_Extra, Self_Consistency)
{
  if (get_mpi_size() == 1) {
    SUCCEED();
    return;
  }

  auto spin_field_array = reserve_wsd(1);
  auto spin_field = spin_field_array[0];

  random_sd(VOLUME, spin_field, 1.0);

  fetch_full_boundary_spins(spin_field);

  /* Multiply boundary by -1 */
  scale_dble(NSPIN - VOLUME, -1.0, spin_field + VOLUME);

  /* Boundary points that will contribute to more than one neighbouring block
   * must be scaled by an appropriate factor */
  for (auto ix = 0; ix < VOLUME; ++ix) {

    /* Count the number of neighbours any point has that is on the halo */
    int bndry_directions = 0;
    for (auto imu = 0; imu < 4; ++imu) {
      bndry_directions += (int)(iup[ix][imu] >= VOLUME);
      bndry_directions += (int)(idn[ix][imu] >= VOLUME);
    }

    /* If is has more than 1 these must be scaled */
    if (bndry_directions > 1) {
      for (auto imu = 0; imu < 4; ++imu) {
        if ((iup[ix][imu] >= VOLUME) and (iup[ix][imu] < NSPIN)) {
          scale_dble(1, 1.0 / bndry_directions, spin_field + iup[ix][imu]);
        } else if ((idn[ix][imu] >= VOLUME) and (idn[ix][imu] < NSPIN)) {
          scale_dble(1, 1.0 / bndry_directions, spin_field + idn[ix][imu]);
        }
      }
    }
  }

  /* Copy the boundary back to where they came from */
  send_full_boundary_spins(spin_field);

  double even_bndry_norm = 0.0;
  double odd_bndry_norm = 0.0;

  for (auto i = 0; i < (BNDRY / 2); ++i) {
    even_bndry_norm += norm_square_dble(1, 0, spin_field + map[i]);
    odd_bndry_norm += norm_square_dble(1, 0, spin_field + map[i + (BNDRY / 2)]);
  }

  release_wsd();

  /* Check that the even boundary is zero */
  EXPECT_NEAR(0.0, even_bndry_norm, 1e-12);

  /* And that the odd is not*/
  EXPECT_FALSE(odd_bndry_norm < 1e-12);
}

/* Check that the full comm reproduces the projected communicator defined for
 * the Dirac operator with the "zero'th" projection operator */
TEST(Spinor_Comm_Extra, Legacy_Consistency_Projection_Zero)
{
  if (get_mpi_size() == 1) {
    SUCCEED();
    return;
  }

  auto spin_field_array = reserve_wsd(1);
  auto spin_field = spin_field_array[0];

  random_sd(VOLUME, spin_field, 1.0);

  fetch_full_boundary_spins(spin_field);

  auto spin_halo =
      (spinor_dble *)amalloc((BNDRY / 2) * sizeof(spinor_dble), ALIGN);

  assign_sd2sd(BNDRY / 2, spin_field + VOLUME, spin_halo);

  /* Use the legacy comm with projection 1 */
  cpsd_int_bnd(0x1, spin_field);

  auto halo_offsets = get_halo_offsets();

  for (auto i = 0; i < BNDRY / 2; ++i) {
    int index_buffer = 0;
    weyl_dble wbuf;

    auto face_id =
        std::distance(halo_offsets.begin(),
                      std::find_if(halo_offsets.begin(), halo_offsets.end(),
                                   [i](int n) { return i < n; })) -
        1;

    assign_sd2wd[face_id](&index_buffer, 1, spin_halo + i, &wbuf);
    unzip_weyl(spin_halo[i], wbuf);
  }

  mulr_spinor_add_dble(BNDRY / 2, spin_halo, spin_field + VOLUME, -1.0);
  release_wsd();

  /* Check all the 8 faces */

  for (auto face = 0; face < 8; ++face) {
    EXPECT_NEAR(
        0.0,
        norm_square_dble(face_sizes[face], 0, spin_halo + halo_offsets[face]),
        1e-12);
  }

  afree(spin_halo);
}

/* Same check but with the first projection operator */
TEST(Spinor_Comm_Extra, Legacy_Consistency_Projection_One)
{
  if (get_mpi_size() == 1) {
    SUCCEED();
    return;
  }

  auto spin_field_array = reserve_wsd(1);
  auto spin_field = spin_field_array[0];

  random_sd(VOLUME, spin_field, 1.0);

  fetch_full_boundary_spins(spin_field);

  auto spin_halo =
      (spinor_dble *)amalloc((BNDRY / 2) * sizeof(spinor_dble), ALIGN);

  assign_sd2sd(BNDRY / 2, spin_field + VOLUME, spin_halo);

  /* Use the legacy comm with projection 1 */
  cpsd_int_bnd(0x0, spin_field);

  auto halo_offsets = get_halo_offsets();

  for (auto i = 0; i < BNDRY / 2; ++i) {
    int index_buffer = 0;
    weyl_dble wbuf;

    auto face_id =
        std::distance(halo_offsets.begin(),
                      std::find_if(halo_offsets.begin(), halo_offsets.end(),
                                   [i](int n) { return i < n; })) -
        1;

    assign_sd2wd[face_id ^ 0x1](&index_buffer, 1, spin_halo + i, &wbuf);
    unzip_weyl(spin_halo[i], wbuf);
  }

  mulr_spinor_add_dble(BNDRY / 2, spin_halo, spin_field + VOLUME, -1.0);
  release_wsd();

  /* Check all the 8 faces */

  for (auto face = 0; face < 8; ++face) {
    EXPECT_NEAR(
        0.0,
        norm_square_dble(face_sizes[face], 0, spin_halo + halo_offsets[face]),
        1e-12);
  }

  afree(spin_halo);
}
