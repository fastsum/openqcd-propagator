
/*
 * Created: 03-06-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef OPENQCD_TEST_FIXTURE_HPP
#define OPENQCD_TEST_FIXTURE_HPP

#include <gtest/gtest.h>

extern "C" {
#include "sources.h"
}

namespace fastsum {
namespace openqcd {

class Sources : public ::testing::Test
{
public:
  void TearDown() override
  {
    openqcd_sources__reset_source_parms();
  }

  virtual ~Sources(){};
};

class Source_Parameters : public Sources
{
public:
  virtual ~Source_Parameters(){};
};

} // namespace openqcd
} // namespace fastsum

#endif /* OPENQCD_TEST_FIXTURE_HPP */
