
/*
 * Created: 23-05-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <gtest/gtest.h>

#include "lib/propagator_archive.c"

extern "C" {
#include "openqcd/c_headers/linalg.h"
#include "openqcd/c_headers/sflds.h"
#include "openqcd/c_headers/su3fcts.h"
#include "openqcd/c_headers/uflds.h"
}

TEST(Propagator_Archive, Write_Read_Random_Field)
{
  auto propagator = propagator_field();
  random_sd(12 * VOLUME, *propagator, 1.0);

  auto tmp_filename = std::string{"tmp_prop.dat"};
  export_propagator(tmp_filename.c_str());

  spinor_dble *tmp_propagator =
      (spinor_dble *)amalloc(12 * VOLUME * sizeof(spinor_dble), ALIGN);
  assign_sd2sd(12 * VOLUME, *propagator, tmp_propagator);

  set_sd2zero(12 * VOLUME, *propagator);
  import_propagator(tmp_filename.c_str());

  std::remove(tmp_filename.c_str());

  mulr_spinor_add_dble(12 * VOLUME, *propagator, tmp_propagator, -1.0);
  ASSERT_NEAR(0.0, norm_square_dble(12 * VOLUME, 0, *propagator), 1e-12);

  afree(tmp_propagator);
}
