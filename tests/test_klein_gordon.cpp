/*
 * Created: 19-09-2018
 * Copyright (c) <2018> <Jonas R. Glesaaen (jonas@glesaaen.com)>
 */

#include "test_utilities.hpp"
#include <gtest/gtest.h>

#include "lib/klein_gordon.c"

extern "C" {
#include "propagator.h"

#include "openqcd/c_headers/dirac.h"
#include "openqcd/c_headers/linalg.h"
#include "openqcd/c_headers/random.h"
#include "openqcd/c_headers/sflds.h"
#include "openqcd/c_headers/su3fcts.h"
#include "openqcd/c_headers/sw_term.h"
#include "openqcd/c_headers/uflds.h"
}

TEST(Klein_Gordon, Temporal_Links_Only)
{
  auto spin_field_array = reserve_wsd(2);
  random_sd(VOLUME, spin_field_array[0], 1.0);

  auto ud = udfld();

  cm3x3_zero(4 * VOLUME, ud);

  for (auto i = 0; i < 4 * VOLUME; i += 8) {
    cm3x3_unity(2, ud + i);
  }

  double mass_sqr = 0.123;

  klein_gordon_spatial(mass_sqr, spin_field_array[0], spin_field_array[1]);

  mulr_spinor_add_dble(VOLUME, spin_field_array[1], spin_field_array[0],
                       -(6. + mass_sqr));
  EXPECT_NEAR(0.0, norm_square_dble(VOLUME, 0, spin_field_array[1]), 1e-12);

  release_wsd();
}

/* Construct the Klein-Gordon operator from the Wilson-Dirac operator
 * It is possible to construct the KG operator by using th Wilson-Dirac operator
 * and some gamma-matrices. Basically (D + g5 D g5) gives you the KG operator
 * with some slightly different prefactors. However, by running them with
 * different masses one can get them to match */
TEST(Klein_Gordon, Dirac_Operator)
{
  auto spin_field_array = reserve_wsd(4);
  random_sd(VOLUME, spin_field_array[0], 1.0);
  random_ud();

  auto ud = udfld();

  for (auto i = 0; i < 4 * VOLUME; i += 8) {
    cm3x3_zero(2, ud + i);
  }

  double mass_sqr = 0.123;
  klein_gordon_spatial(mass_sqr, spin_field_array[0], spin_field_array[1]);

  /* (D + g5 D g5) gives two times its diagonal contribution, as we want to
   * match up (6 + m) from KG with 2 * (4 + m) from Dirac, we choose the masses
   * so that these match */
  set_sw_parms(0.5 * mass_sqr - 1.0);
  sw_term(NO_PTS);

  /* D psi stored in prop[4] */
  Dw_dble(0.0, spin_field_array[0], spin_field_array[2]);

  /* g5 D g5 psi stored in prop[6] */
  mulg5_dble(VOLUME, spin_field_array[0]);
  Dw_dble(0.0, spin_field_array[0], spin_field_array[3]);
  mulg5_dble(VOLUME, spin_field_array[3]);

  /* Add (D + g5 D g5) */
  mulr_spinor_add_dble(VOLUME, spin_field_array[2], spin_field_array[3], 1.0);

  /* Subtract result from that from KG */
  mulr_spinor_add_dble(VOLUME, spin_field_array[1], spin_field_array[2], -1.0);

  /* Check that it is zero */
  EXPECT_NEAR(0.0, norm_square_dble(VOLUME, 0, spin_field_array[1]), 1e-12);

  release_wsd();
}
