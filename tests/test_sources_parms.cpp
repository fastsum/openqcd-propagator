/*
 * Created: 03-06-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#include <algorithm>
#include <fstream>
#include <initializer_list>
#include <vector>

#include <gmock/gmock.h>

#include "lib/sources_parms.c"
#include "openqcd_test_fixture.hpp"
#include "test_utilities.hpp"

extern "C" {
#include "openqcd/c_headers/utils.h"
#include "propagator.h"
}

MATCHER_P(IsSameSource, source, "")
{
  if (arg.type != source.type) {
    return false;
  }

  for (auto i = 0; i < 4; ++i) {
    if (arg.pos[i] != source.pos[i]) {
      return false;
    }
  }

  if (arg.type == POINT_SOURCE) {
    return true;
  }

  return (arg.num_application == source.num_application) and
         (arg.smear_field == source.smear_field) and
         (arg.smear_parameter == source.smear_parameter);
}

void print_source(std::ostream &os, source_parms_t sp, int random, int sid)
{
  os << "[Source " << sid << "]\n";

  if (random) {
    os << "point RANDOM\n";
  } else {
    os << "point " << sp.pos[0] << " " << sp.pos[1] << " " << sp.pos[2] << " "
       << sp.pos[3] << "\n";
  }

  switch (sp.type) {
  case POINT_SOURCE:
    os << "source POINT_SOURCE\n";
    break;
  case GAUSSIAN_SMEARED_SOURCE:
    os << "source GAUSSIAN_SMEARED_SOURCE\n"
       << "smear_gauge " << (sp.smear_field != 0) << "\n"
       << "num " << sp.num_application << "\n"
       << "kappa " << sp.smear_parameter << "\n";
    break;
  default:
    std::runtime_error{"In print_source: Invalid source type"};
  }

  os << "\n";
}

template <typename Type, std::size_t N>
void copy(Type to[N], std::initializer_list<Type> from)
{
  if (N != from.size()) {
    throw std::runtime_error{"Trying to copy from an initializer_list of "
                             "different length than the target array"};
  }

  std::copy(from.begin(), from.end(), to);
}

void set_random(source_parms_t sps[], int nsrc)
{
  std::vector<int> random_points(4 * nsrc);

  if (get_mpi_rank() == 0) {
    std::vector<int> ranlux_state(rlxs_size());
    rlxs_get(ranlux_state.data());

    std::vector<float> rng_floats(4 * nsrc);
    ranlxs(rng_floats.data(), 4 * nsrc);

    for (auto i = 0; i < nsrc; ++i) {
      random_points[4 * i + 0] =
          static_cast<int>(rng_floats[4 * i + 0] * N0) % (N0);
      random_points[4 * i + 1] =
          static_cast<int>(rng_floats[4 * i + 1] * N1) % (N1);
      random_points[4 * i + 2] =
          static_cast<int>(rng_floats[4 * i + 2] * N2) % (N2);
      random_points[4 * i + 3] =
          static_cast<int>(rng_floats[4 * i + 3] * N3) % (N3);
    }

    rlxs_reset(ranlux_state.data());
  }

  if (NPROC > 1) {
    MPI_Bcast(random_points.data(), 4 * nsrc, MPI_FLOAT, 0, MPI_COMM_WORLD);
  }

  for (auto i = 0; i < nsrc; ++i) {
    for (auto j = 0; j < 4; ++j) {
      sps[i].pos[j] = random_points[4 * i + j];
    }
  }
}

void place_in_corners(source_parms_t sps[])
{
  for (auto i = 0; i < 15; ++i) {
    sps[i+1].pos[0] = (sps[0].pos[0] + corner_order[i][0] * (N0 / 2)) % N0; 
    sps[i+1].pos[1] = (sps[0].pos[1] + corner_order[i][1] * (N1 / 2)) % N1; 
    sps[i+1].pos[2] = (sps[0].pos[2] + corner_order[i][2] * (N2 / 2)) % N2; 
    sps[i+1].pos[3] = (sps[0].pos[3] + corner_order[i][3] * (N3 / 2)) % N3; 
  }
}

using namespace fastsum::openqcd;

TEST_F(Source_Parameters, Setting_Sources)
{
  source_parms_t sp = {POINT_SOURCE, {0, 0, 0, 0}, 0, 0, 0.0};

  set_source_parms(0, sp.type, sp.pos, 0, sp.smear_field, sp.num_application,
                   sp.smear_parameter);

  auto get_sp = source_parms(0);
  EXPECT_THAT(get_sp, IsSameSource(sp));

  sp = {GAUSSIAN_SMEARED_SOURCE, {0, 2, 0, 2}, 1, 10, 0.1};
  set_source_parms(1, sp.type, sp.pos, 0, sp.smear_field, sp.num_application,
                   sp.smear_parameter);

  get_sp = source_parms(1);
  EXPECT_THAT(get_sp, IsSameSource(sp));
}

TEST_F(Source_Parameters, Looping_Sources)
{
  source_parms_t sp = {GAUSSIAN_SMEARED_SOURCE, {0, 2, 0, 2}, 1, 10, 0.1};
  set_source_parms(0, sp.type, sp.pos, 0, sp.smear_field, sp.num_application,
                   sp.smear_parameter);
  set_source_parms(4, sp.type, sp.pos, 0, sp.smear_field, sp.num_application,
                   sp.smear_parameter);
  set_source_parms(22, sp.type, sp.pos, 0, sp.smear_field, sp.num_application,
                   sp.smear_parameter);

  auto isrc = -1;

  isrc = next_source_id(isrc + 1);
  EXPECT_EQ(0, isrc);
  EXPECT_THAT(source_parms(isrc), IsSameSource(sp));

  isrc = next_source_id(isrc + 1);
  EXPECT_EQ(4, isrc);
  EXPECT_THAT(source_parms(isrc), IsSameSource(sp));

  isrc = next_source_id(isrc + 1);
  EXPECT_EQ(22, isrc);
  EXPECT_THAT(source_parms(isrc), IsSameSource(sp));

  isrc = next_source_id(isrc + 1);
  EXPECT_EQ(-1, isrc);
}

TEST_F(Source_Parameters, Parsing_Sources)
{
  source_parms_t sp0 = {GAUSSIAN_SMEARED_SOURCE, {0, 2, 0, 2}, 1, 10, 0.1};
  source_parms_t sp7 = {POINT_SOURCE, {6, 2, 6, 2}, 0, 0, 0.0};
  source_parms_t sp22 = {GAUSSIAN_SMEARED_SOURCE, {0, 0, 0, 0}, 0, 7, 1.5};

  auto filename = std::string{"source_parameters_parsing_source.in"};
  FILE *fin = nullptr;

  auto mpi_rank = get_mpi_rank();

  if (mpi_rank == 0) {
    std::ofstream ofs{filename};

    print_source(ofs, sp0, 0, 0);
    print_source(ofs, sp7, 0, 7);
    print_source(ofs, sp22, 0, 22);

    ofs.close();

    fin = std::freopen(filename.c_str(), "r", stdin);
  }

  read_source_parms(0);
  read_source_parms(7);
  read_source_parms(22);

  if (mpi_rank == 0) {
    fclose(fin);
    std::remove(filename.c_str());
  }

  auto isrc = -1;
  isrc = next_source_id(isrc + 1);
  EXPECT_EQ(0, isrc);
  EXPECT_THAT(source_parms(isrc), IsSameSource(sp0));

  isrc = next_source_id(isrc + 1);
  EXPECT_EQ(7, isrc);
  EXPECT_THAT(source_parms(isrc), IsSameSource(sp7));

  isrc = next_source_id(isrc + 1);
  EXPECT_EQ(22, isrc);
  EXPECT_THAT(source_parms(isrc), IsSameSource(sp22));

  isrc = next_source_id(isrc + 1);
  EXPECT_EQ(-1, isrc);
}

TEST_F(Source_Parameters, Random_Sources_Random_Distribution)
{
  source_parms_t sps[] = {{POINT_SOURCE, {0, 0, 0, 0}, 0, 0, 0.0},
                          {GAUSSIAN_SMEARED_SOURCE, {0, 0, 0, 0}, 1, 10, 5.0},
                          {POINT_SOURCE, {0, 0, 0, 0}, 0, 0, 0.0},
                          {GAUSSIAN_SMEARED_SOURCE, {0, 0, 0, 0}, 0, 56, 0.25}};

  start_ranlux(0, 12345);

  std::vector<int> ranlux_state(rlxs_size());
  rlxs_get(ranlux_state.data());

  set_random(sps, 4);

  for (auto i = 0; i < 4; ++i) {
    set_source_parms(i, sps[i].type, sps[i].pos, 1, sps[i].smear_field,
                     sps[i].num_application, sps[i].smear_parameter);
  }

  distribute_random_sources(RANDOM_DISTRIBUTION, 0);

  for (auto i = 0; i < 4; ++i) {
    EXPECT_THAT(source_parms(i), IsSameSource(sps[i]));
  }

  std::vector<int> updated_ranlux_state(rlxs_size());
  rlxs_get(updated_ranlux_state.data());

  /* Test that the randomisation leaves the ranlux state untouched */
  EXPECT_EQ(updated_ranlux_state, ranlux_state);
}

TEST_F(Source_Parameters, Random_Sources_Max_Distance_Distribution)
{
  source_parms_t sps[] = {{POINT_SOURCE, {0, 0, 0, 0}, 0, 0, 0.0},
                          {GAUSSIAN_SMEARED_SOURCE, {0, 0, 0, 0}, 1, 10, 5.0},
                          {POINT_SOURCE, {0, 0, 0, 0}, 0, 0, 0.0},
                          {GAUSSIAN_SMEARED_SOURCE, {0, 0, 0, 0}, 0, 56, 0.25},
                          {POINT_SOURCE, {0, 0, 0, 0}, 0, 0, 0.0},
                          {GAUSSIAN_SMEARED_SOURCE, {0, 0, 0, 0}, 1, 10, 5.0},
                          {POINT_SOURCE, {0, 0, 0, 0}, 0, 0, 0.0},
                          {GAUSSIAN_SMEARED_SOURCE, {0, 0, 0, 0}, 0, 56, 0.25},
                          {POINT_SOURCE, {0, 0, 0, 0}, 0, 0, 0.0},
                          {GAUSSIAN_SMEARED_SOURCE, {0, 0, 0, 0}, 1, 10, 5.0},
                          {POINT_SOURCE, {0, 0, 0, 0}, 0, 0, 0.0},
                          {GAUSSIAN_SMEARED_SOURCE, {0, 0, 0, 0}, 0, 56, 0.25},
                          {POINT_SOURCE, {0, 0, 0, 0}, 0, 0, 0.0},
                          {GAUSSIAN_SMEARED_SOURCE, {0, 0, 0, 0}, 1, 10, 5.0},
                          {POINT_SOURCE, {0, 0, 0, 0}, 0, 0, 0.0},
                          {GAUSSIAN_SMEARED_SOURCE, {0, 0, 0, 0}, 0, 56, 0.25}};

  start_ranlux(0, 12345);

  std::vector<int> ranlux_state(rlxs_size());
  rlxs_get(ranlux_state.data());

  set_random(sps, 1);
  place_in_corners(sps);

  for (auto i = 0; i < 16; ++i) {
    set_source_parms(i, sps[i].type, sps[i].pos, 1, sps[i].smear_field,
                     sps[i].num_application, sps[i].smear_parameter);
  }

  distribute_random_sources(MAX_DISTANCE_DISTRIBUTION, 0);

  for (auto i = 0; i < 16; ++i) {
    EXPECT_THAT(source_parms(i), IsSameSource(sps[i]));
  }

  std::vector<int> updated_ranlux_state(rlxs_size());
  rlxs_get(updated_ranlux_state.data());

  /* Test that the randomisation leaves the ranlux state untouched */
  EXPECT_EQ(updated_ranlux_state, ranlux_state);
}

TEST_F(Source_Parameters, Random_Source_Infile)
{
  source_parms_t sp0 = {GAUSSIAN_SMEARED_SOURCE, {0, 0, 0, 0}, 1, 10, 0.1};
  source_parms_t sp7 = {POINT_SOURCE, {0, 0, 0, 0}, 0, 0, 0.0};
  source_parms_t sp22 = {GAUSSIAN_SMEARED_SOURCE, {0, 0, 0, 0}, 0, 7, 1.5};

  auto filename = std::string{"source_parameters_parsing_source.in"};
  FILE *fin = nullptr;

  auto mpi_rank = get_mpi_rank();

  if (mpi_rank == 0) {
    std::ofstream ofs{filename};

    print_source(ofs, sp0, 1, 0);
    print_source(ofs, sp7, 1, 7);
    print_source(ofs, sp22, 1, 22);

    ofs.close();

    fin = std::freopen(filename.c_str(), "r", stdin);
  }

  read_source_parms(0);
  read_source_parms(7);
  read_source_parms(22);

  if (mpi_rank == 0) {
    fclose(fin);
    std::remove(filename.c_str());
  }

  start_ranlux(0, 12345);

  source_parms_t sps[3];
  set_random(sps, 3);

  distribute_random_sources(RANDOM_DISTRIBUTION, 1);

  std::copy(sps[0].pos, sps[0].pos + 4, sp0.pos);
  std::copy(sps[1].pos, sps[1].pos + 4, sp7.pos);
  std::copy(sps[2].pos, sps[2].pos + 4, sp22.pos);

  auto isrc = -1;
  isrc = next_source_id(isrc + 1);
  EXPECT_EQ(0, isrc);
  EXPECT_THAT(source_parms(isrc), IsSameSource(sp0));

  isrc = next_source_id(isrc + 1);
  EXPECT_EQ(7, isrc);
  EXPECT_THAT(source_parms(isrc), IsSameSource(sp7));

  isrc = next_source_id(isrc + 1);
  EXPECT_EQ(22, isrc);
  EXPECT_THAT(source_parms(isrc), IsSameSource(sp22));

  isrc = next_source_id(isrc + 1);
  EXPECT_EQ(-1, isrc);
}

TEST_F(Source_Parameters, Random_Update)
{
  source_parms_t sp = {POINT_SOURCE, {0, 0, 0, 0}, 0, 0, 0.0};

  start_ranlux(0, 12345);

  std::vector<int> ranlux_init_state(rlxs_size());
  rlxs_get(ranlux_init_state.data());

  std::vector<int> ranlux_incr_state(rlxs_size());

  /* First test a max-distance distribution, should pick 4 random numbers */

  float f[16];
  ranlxs(f, 4);

  rlxs_get(ranlux_incr_state.data());
  rlxs_reset(ranlux_init_state.data());

  for (auto i = 0; i < 4; ++i) {
    set_source_parms(i, sp.type, sp.pos, 1, sp.smear_field, sp.num_application,
                     sp.smear_parameter);
  }

  distribute_random_sources(MAX_DISTANCE_DISTRIBUTION, 1);

  /* Only rank 0 will update its ranlux state */
  std::vector<int> updated_ranlux_state(rlxs_size());
  rlxs_get(updated_ranlux_state.data());

  if (get_mpi_rank() == 0) {
    EXPECT_EQ(updated_ranlux_state, ranlux_incr_state);
  } else {
    EXPECT_EQ(updated_ranlux_state, ranlux_init_state);
  }

  /* Test a random distribution, should pick 16 random numbers */

  rlxs_reset(ranlux_init_state.data());
  ranlxs(f, 16);

  rlxs_get(ranlux_incr_state.data());
  rlxs_reset(ranlux_init_state.data());

  distribute_random_sources(RANDOM_DISTRIBUTION, 1);
  rlxs_get(updated_ranlux_state.data());

  /* Only rank 0 will update its ranlux state */
  if (get_mpi_rank() == 0) {
    EXPECT_EQ(updated_ranlux_state, ranlux_incr_state);
  } else {
    EXPECT_EQ(updated_ranlux_state, ranlux_init_state);
  }
}

TEST_F(Source_Parameters, Random_Noupdate)
{
  source_parms_t sp = {POINT_SOURCE, {0, 0, 0, 0}, 0, 0, 0.0};

  start_ranlux(0, 12345);

  std::vector<int> ranlux_init_state(rlxs_size());
  rlxs_get(ranlux_init_state.data());

  for (auto i = 0; i < 4; ++i) {
    set_source_parms(i, sp.type, sp.pos, 1, sp.smear_field, sp.num_application,
                     sp.smear_parameter);
  }

  /* Test a max-distance distribution */
  distribute_random_sources(MAX_DISTANCE_DISTRIBUTION, 0);

  std::vector<int> updated_ranlux_state(rlxs_size());
  rlxs_get(updated_ranlux_state.data());

  EXPECT_EQ(updated_ranlux_state, ranlux_init_state);

  /* Test a random distribution, should pick 16 random numbers */
  distribute_random_sources(RANDOM_DISTRIBUTION, 0);
  rlxs_get(updated_ranlux_state.data());

  EXPECT_EQ(updated_ranlux_state, ranlux_init_state);
}
