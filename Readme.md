# openQCD Propagator Computation Program and Library

openqcd-propagator provides functions for computing propagators and propagator
related quantities using the openqcd framework. It builds on top of
openqcd-fastsum v1.1 (using the library feature), and will therefore inherit all
of its performance.

Implemented sources:
 - point sources
 - spatially Gaussian smeared sources

Solves carried over from openqcd:
 - CG solver
 - SAP GCR
 - Deflated SAP GCR

## Building

To compile the project simply `cd` into the `build` directory and run `make`.
See the Readme file inside of the `build` directory for more details.

### Requirements

 * v1.1 of [openqcd-fastsum][openqcd-fastsum lib]
 * an implementation of MPI

## Usage

The source contains two components that can be built, the library and the
executable.

### The executable

The executable is a program that runs through a list of existing openQCD
configurations, and computes and stores a propagator for that configuration to
disk. The program has the following usage:

```
usage: openqcd-propagator -i <input file> [-a] [-seed <seed>]

Program Parameters:
-i <input file>  Name of the input file from which the simulation parameters are
                 read

Optional parameters:
-a               Run in append mode to continue a previously started run from
                 the last successful computation
-seed <seed>     Set the simulation RNG seed, overriding the infile seed and old
                 seeds in continuation runs
```

The program infile is has many of the same sections as openqcd, and an example
of a complete infile can be found at `main/examples/propagator.in`. The section
that is new concerns specifying sources for the propagator computation and takes
the form:

```
[Sources]
num_sources  3
random_dist  MAX_DISTANCE

[Source 0]
source  POINT_SOURCE
point   0 0 0 0

[Source 1]
source       GAUSSIAN_SMEARED_SOURCE
point        RANDOM
smear_gauge  1
num          60
kappa        4.2

[Source 2]
source  POINT_SOURCE
point   RANDOM
```

The program supports a maximum of 32 sources for a single computation. More
information about the source parameters is in the [project
documentation](doc/propagator.pdf).

### The library

The code also makes useful internal functions available in a library format.
These include functionality such as the ability to smear propagator fields,
setting up sources, etc.

The API documentation is mostly in the headers of the `*.c` files in the `lib`
directory. On top of the C API a set of C++ headers are also included which wrap
the C functions in an appropriate namespace.

### Tests

The project also comes with unit tests which can be compiled with the makefile.
These tests use the Googletest unit testing framework which has to be
downloaded. This can be done by fetching it as a submodule to this project.

```
git submodule update --init --recursive
```

This is not a dependency of the library/executable and does therefore only have
to be downloaded if one wants to run the tests.

The test executable is named `run_tests` and has the following usage:

```
Usage: run_tests [processor_grid]

Optional parameters:
 - processor_grid (str) -- Nt,Nx,Ny,Nz     A comma separated list of the number
                                           of cores in each of the four space-
                                           time directions.
```

The tests all run with a local lattice size of 8x8x8x8, meaning that running
them in parallel will only increase the global volume. If no grid is provided,
the program will try to create one based on the MPI size. The only restriction
is that the number of threads must be an even number. If you want to test a
specific partitioning, it must be specified using the `processor_grid` argument.

## Authors

This code was written by Jonas Rylund Glesaaen while a post-doc in the lattice
group at Swansea University.

## License

The software may be used under the terms of the GNU General Public Licence
(GPL).

[openqcd-fastsum lib]: https://gitlab.com/fastsum/openqcd-fastsum
