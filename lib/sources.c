/*******************************************************************************
 *
 * File sources.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * The externally accessible functions are
 *
 *   void generate_source(int isrc, int spin_colour_index, spinor_dble *psi)
 *     Generate a source of the type specified by the source index and store it
 *     in the spinor psi. One also wants to specify the spin-colour index which
 *     will determine which component will be the seed for the point source in
 *     the case that the source needs one specified.
 *
 *******************************************************************************/

#define SOURCES_C
#define OPENQCD_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif

#include "sources.h"
#include "klein_gordon.h"
#include "smearing.h"

#include "openqcd/c_headers/dirac.h"
#include "openqcd/c_headers/global.h"
#include "openqcd/c_headers/linalg.h"
#include "openqcd/c_headers/sflds.h"
#include "openqcd/c_headers/stout_smearing.h"
#include "openqcd/c_headers/utils.h"

#ifdef __cplusplus
}
#endif

/* Create an array with the local lattice size. I feel like I need this
 * functionality often enough that it would warrant actually having it as a
 * global array instead of 4 constants */
static void local_lattice_size(int *lsize)
{
  lsize[0] = L0;
  lsize[1] = L1;
  lsize[2] = L2;
  lsize[3] = L3;
}

/* Given a point specified by cartesian coordinates, return the openqcd site
 * index */
static int local_lattice_site(int pt[4])
{
  return ipt[pt[3] + L3 * (pt[2] + L2 * (pt[1] + L1 * pt[0]))];
}

/* Check whether a point specified in the global coordinate system lies in this
 * processor's local lattice */
static int point_on_local_lattice(int point[4], int lidx[4])
{
  int i, lsizes[4];
  local_lattice_size(lsizes);

  for (i = 0; i < 4; ++i) {
    lidx[i] = point[i] - lsizes[i] * cpr[i];

    if ((lidx[i] < 0) || (lidx[i] >= lsizes[i])) {
      return 0;
    }
  }

  return 1;
}

void generate_source(int isrc, int spin_colour_index, spinor_dble *psi)
{
  int lidx[4];
  double *spinor_value;
  source_parms_t sp;

  sp = source_parms(isrc);
  set_sd2zero(VOLUME, psi);

  if (point_on_local_lattice(sp.pos, lidx)) {
    spinor_value = (double *)(psi + local_lattice_site(lidx));
    spinor_value[2 * spin_colour_index] = 1.0;
  }

  if (sp.type == GAUSSIAN_SMEARED_SOURCE) {
    if (sp.smear_field) {
      smear_fields();
    }

    gaussian_smearing(psi, sp.num_application, sp.smear_parameter);

    if (sp.smear_field) {
      unsmear_fields();
    }
  }
}
