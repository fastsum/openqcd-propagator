/*******************************************************************************
 *
 * File sdcom_extra.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * The externally accessible functions are
 *
 *   void fetch_full_boundary_spins(spinor_dble *psi)
 *     Add the value of neighbouring spinor halos at even lattice points to the
 *     corresponding points in the local volume. The full spinor equivalent of
 *     cpsd_ext_bnd.
 *
 *   void send_full_boundary_spins(spinor_dble *psi)
 *     Copies the values of even interior lattice points to neighbouring spinor
 *     halos. This is the full spinor equivalent of cpsd_int_bnd.
 *
 * Notes:
 *
 *   The methods in this module exists because the functionality provided by the
 *   sdcom module in openqcd-fastsum only copies half of the spinor content as
 *   one does not need the full spinor content when computing the hopping
 *   contribution to the Wilson-Dirac operator. However, when implementing the
 *   Klein-Gordon operator this information is necessary.
 *
 *******************************************************************************/

#define SDCOM_EXTRA_C
#define OPENQCD_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif

#include "sdcom_extra.h"

#include "openqcd/c_headers/flags.h"
#include "openqcd/c_headers/global.h"
#include "openqcd/c_headers/lattice.h"
#include "openqcd/c_headers/linalg.h"
#include "openqcd/c_headers/sflds.h"

#ifdef __cplusplus
}
#endif

#include "mpi.h"

int odd_block = 0;
int nspins_face[8], offsets_face[8];
spinor_dble *comm_buffer = NULL, *send_buffer, *recieve_buffer;

static void alloc_sdcom_bufs(void)
{
  int i, buffer_size = 0;

  odd_block = (cpr[0] + cpr[1] + cpr[2] + cpr[3]) & 0x1;

  if (NPROC == 1) {
    return;
  }

  nspins_face[0] = FACE0 / 2;
  nspins_face[1] = FACE0 / 2;
  nspins_face[2] = FACE1 / 2;
  nspins_face[3] = FACE1 / 2;
  nspins_face[4] = FACE2 / 2;
  nspins_face[5] = FACE2 / 2;
  nspins_face[6] = FACE3 / 2;
  nspins_face[7] = FACE3 / 2;

  offsets_face[0] = 0;
  for (i = 1; i < 8; ++i) {
    if (buffer_size < nspins_face[i]) {
      buffer_size = nspins_face[i];
    }

    offsets_face[i] = offsets_face[i - 1] + nspins_face[i - 1];
  }

  comm_buffer =
      (spinor_dble *)amalloc(buffer_size * sizeof(spinor_dble), ALIGN);

  error(comm_buffer == NULL, 1, "allocate_sdcom_bufs [sdcom_extra.c]",
        "Unable to allocate send recieve buffer");
}

static void pack_face(int ifc, spinor_dble const *psi)
{
  int *idx;
  spinor_dble *s, *sm;

  /* ^ 0x1 is a bit-flip of the first bit
   * For faces this basically means flip the face to be the opposite face
   * pointing in the minus direction. When we pack the face we need to pack
   * from the oposite direction we are sending to */
  s = comm_buffer;
  sm = comm_buffer + nspins_face[ifc];
  idx = map + offsets_face[ifc ^ 0x1];

  for (; s < sm; ++s, ++idx) {
    (*s) = psi[*idx];
  }
}

static void unpack_face(int ifc, spinor_dble *psi)
{
  int *idx;
  spinor_dble *s, *sm;

  s = comm_buffer;
  sm = comm_buffer + nspins_face[ifc ^ 0x1];
  idx = map + offsets_face[ifc];

  for (; s < sm; ++s, ++idx) {
    mulr_spinor_add_dble(1, psi + (*idx), s, 1.0);
  }
}

static void communicate_face(int ifc)
{
  int tag, saddr, raddr;
  MPI_Status stat;

  tag = mpi_tag();
  saddr = npr[ifc];
  raddr = npr[ifc ^ 0x1];

  if (odd_block == 0) {
    MPI_Send(send_buffer, 24 * nspins_face[ifc], MPI_DOUBLE, saddr, tag,
             MPI_COMM_WORLD);
    MPI_Recv(recieve_buffer, 24 * nspins_face[ifc], MPI_DOUBLE, raddr, tag,
             MPI_COMM_WORLD, &stat);
  } else {
    MPI_Recv(recieve_buffer, 24 * nspins_face[ifc], MPI_DOUBLE, raddr, tag,
             MPI_COMM_WORLD, &stat);
    MPI_Send(send_buffer, 24 * nspins_face[ifc], MPI_DOUBLE, saddr, tag,
             MPI_COMM_WORLD);
  }
}

void fetch_full_boundary_spins(spinor_dble *psi)
{
  int ifc;

  if (comm_buffer == NULL) {
    alloc_sdcom_bufs();
  }

  for (ifc = 0; ifc < 8; ++ifc) {
    if (nspins_face[ifc] == 0) {
      continue;
    }

    pack_face(ifc, psi);

    send_buffer = comm_buffer;
    recieve_buffer = psi + VOLUME + offsets_face[ifc ^ 0x1];
    communicate_face(ifc);
  }
}

void send_full_boundary_spins(spinor_dble *psi)
{
  int ifc;

  if (comm_buffer == NULL) {
    alloc_sdcom_bufs();
  }

  for (ifc = 0; ifc < 8; ++ifc) {
    if (nspins_face[ifc] == 0) {
      continue;
    }

    recieve_buffer = comm_buffer;
    send_buffer = psi + VOLUME + offsets_face[ifc];
    communicate_face(ifc);

    unpack_face(ifc, psi);
  }
}
