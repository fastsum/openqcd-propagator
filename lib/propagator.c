/*******************************************************************************
 *
 * File propagator.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * The externally accessible functions are
 *   void solve_dirac(spinor_dble *source, spinor_dble *target, int isolv,
 *                    int *status)
 *     Solves the Dirac equation Dw source = target using the solver registered
 *     at isolv, storing the status of the solve in status.
 *
 *   timing_info_t compute_propagator(int isrc, int *status)
 *     Computes the full spin-colour propagator for the source object registered
 *     at ID isrc and stores the solve status in status. The propagator itself
 *     is stored in the globally accessible propagator_field(). The return
 *     value gives the timings of the solves.
 *
 *******************************************************************************/

#define PROPAGATOR_C
#define OPENQCD_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif

#include "propagator.h"
#include "sources.h"

#include "openqcd/c_headers/dfl.h"
#include "openqcd/c_headers/dirac.h"
#include "openqcd/c_headers/flags.h"
#include "openqcd/c_headers/forces.h"
#include "openqcd/c_headers/linalg.h"
#include "openqcd/c_headers/sap.h"
#include "openqcd/c_headers/sflds.h"
#include "openqcd/c_headers/stout_smearing.h"
#include "openqcd/c_headers/utils.h"

#ifdef __cplusplus
}
#endif

#include <mpi.h>

static int my_rank = 0;

void solve_dirac(spinor_dble *source, spinor_dble *target, int isolv,
                 int *status)
{
  dirac_operator_parms_t dop;
  solver_parms_t sp;
  sap_parms_t sap;

  dop = dirac_operator_parms();
  sp = solver_parms(isolv);

  if (sp.solver == CGNE) {
    mulg5_dble(VOLUME, source);

    tmcg(sp.nmx, sp.res, dop.mu, source, source, status);

    error_root(status[0] < 0, 1, "solve_dirac [propagator.c]",
               "CGNE solver failed (status = %d)", status[0]);

    Dw_dble(-dop.mu, source, target);
    mulg5_dble(VOLUME, target);
  } else if (sp.solver == SAP_GCR) {
    sap = sap_parms();
    set_sap_parms(sap.bs, sp.isolv, sp.nmr, sp.ncy);

    sap_gcr(sp.nkv, sp.nmx, sp.res, dop.mu, source, target, status);

    error_root(status[0] < 0, 1, "solve_dirac [propagator.c]",
               "SAP_GCR solver failed (status = %d)", status[0]);
  } else if (sp.solver == DFL_SAP_GCR) {
    sap = sap_parms();
    set_sap_parms(sap.bs, sp.isolv, sp.nmr, sp.ncy);

    dfl_sap_gcr2(sp.nkv, sp.nmx, sp.res, dop.mu, source, target, status);

    error_root((status[0] < 0) || (status[1] < 0), 1,
               "solve_dirac [propagator.c]",
               "DFL_SAP_GCR solver failed "
               "(status = %d,%d,%d)",
               status[0], status[1], status[2]);
  } else {
    error_root(1, 1, "solve_dirac [propagator.c]",
               "Unknown or unsupported solver");
  }
}

timing_info_t compute_propagator(int isrc, int *status)
{
  int sci, l, stat[3];
  spinor_dble *eta, **wsd, **prop_field;
  double start_time;
  timing_info_t tinfo = {0.0, 0.0, 0};
  dirac_operator_parms_t dop;

  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

  prop_field = propagator_field();
  wsd = reserve_wsd(1);
  eta = wsd[0];
  dop = dirac_operator_parms();

  /* Reset the inversion status */
  for (l = 0; l < 3; l++) {
    status[l] = 0;
    stat[l] = 0;
  }

  /* Loop over the spin and colour indices */
  for (sci = 0; sci < 12; ++sci) {
    start_time = start_timer();

    generate_source(isrc, sci, eta);

    if (dop.smear) {
      smear_fields();
    }

    solve_dirac(eta, prop_field[sci], 0, stat);

    if (dop.smear) {
      unsmear_fields();
    }

    stop_timer(&tinfo, start_time);

    for (l = 0; l < 2; l++) {
      status[l] += stat[l];
    }

    status[2] += (stat[2] != 0);
  }

  for (l = 0; l < 2; l++) {
    status[l] = (status[l] + (12 / 2)) / 12;
  }

  release_wsd();

  return tinfo;
}
