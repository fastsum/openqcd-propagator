/*******************************************************************************
 *
 * File klein_gordon.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * The externally accessible functions are
 *
 *   void klein_gordon_spatial(double mass_sqr, spinor_dble *psi,
 *                             spinor_dble *eta)
 *     Applies the spatial Klein-Gordon operator to spinor field psi and stores
 *     the result in eta. The operator is defined as (D^2 + m^2), where D^2 is
 *     only summed over the three spatial directions.
 *
 * Notes:
 *
 *   The Klein-Gordon operator is not properly const correct in a similar way to
 *   how it is for the Dirac operator in openQCD. This is because the halo of
 *   psi needs to be modified by the operator to guarantee correctness.
 *
 *******************************************************************************/

#define KLEIN_GORDON_C
#define OPENQCD_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif

#include "klein_gordon.h"
#include "sdcom_extra.h"

#include "openqcd/c_headers/dirac.h"
#include "openqcd/c_headers/global.h"
#include "openqcd/c_headers/lattice.h"
#include "openqcd/c_headers/linalg.h"
#include "openqcd/c_headers/sflds.h"
#include "openqcd/c_headers/uflds.h"

#ifdef __cplusplus
}
#endif

typedef union
{
  spinor_dble s;
  weyl_dble w[2];
} spin_t;

static double coe, ceo;
static spin_t rs ALIGNED32;

#define _vector_mul_assign(r, c)                                               \
  (r).c1.re *= (c);                                                            \
  (r).c1.im *= (c);                                                            \
  (r).c2.re *= (c);                                                            \
  (r).c2.im *= (c);                                                            \
  (r).c3.re *= (c);                                                            \
  (r).c3.im *= (c)

/* Compute to = u * from, for each spinor index of the to and from spinors */
static void su3_multiply_add(spinor_dble *to, su3_dble const *u,
                             spinor_dble const *from)
{
  su3_vector_dble psi;

  _su3_multiply(psi, *u, (*from).c1);
  _vector_add_assign((*to).c1, psi);

  _su3_multiply(psi, *u, (*from).c2);
  _vector_add_assign((*to).c2, psi);

  _su3_multiply(psi, *u, (*from).c3);
  _vector_add_assign((*to).c3, psi);

  _su3_multiply(psi, *u, (*from).c4);
  _vector_add_assign((*to).c4, psi);
}

/* Compute to = u^-1 * from, for each spinor index of the to and from spinors */
static void su3_inverse_multiply_add(spinor_dble *to, su3_dble const *u,
                                     spinor_dble const *from)
{
  su3_vector_dble psi;

  _su3_inverse_multiply(psi, *u, (*from).c1);
  _vector_add_assign((*to).c1, psi);

  _su3_inverse_multiply(psi, *u, (*from).c2);
  _vector_add_assign((*to).c2, psi);

  _su3_inverse_multiply(psi, *u, (*from).c3);
  _vector_add_assign((*to).c3, psi);

  _su3_inverse_multiply(psi, *u, (*from).c4);
  _vector_add_assign((*to).c4, psi);
}

/* Similar to how the Dirac operator is implemented in openQCD we compute the
 * nearest-neighbour contributions in two stages. This is the first one collects
 * contributions from neighbours into the currently active point (which is odd).
 */
static void doe_spatial_dble(int *piup, int *pidn, su3_dble const *u,
                             spinor_dble const *pk)
{
  spinor_dble const *sp, *sm;

  /* Skip direction 0 */
  piup += 1;
  pidn += 1;
  u += 1;

  /****************************** direction +1 ********************************/

  sp = pk + (*(piup++));
  u += 1;

  _su3_multiply(rs.s.c1, *u, (*sp).c1);
  _su3_multiply(rs.s.c2, *u, (*sp).c2);
  _su3_multiply(rs.s.c3, *u, (*sp).c3);
  _su3_multiply(rs.s.c4, *u, (*sp).c4);

  /****************************** direction -1 ********************************/

  sm = pk + (*(pidn++));
  u += 1;

  su3_inverse_multiply_add(&rs.s, u, sm);

  /****************************** direction +2 ********************************/

  sp = pk + (*(piup++));
  u += 1;

  su3_multiply_add(&rs.s, u, sp);

  /****************************** direction -2 ********************************/

  sm = pk + (*(pidn++));
  u += 1;

  su3_inverse_multiply_add(&rs.s, u, sm);

  /****************************** direction +3 ********************************/

  sp = pk + (*(piup));
  u += 1;

  su3_multiply_add(&rs.s, u, sp);

  /****************************** direction -3 ********************************/

  sm = pk + (*(pidn));
  u += 1;

  su3_inverse_multiply_add(&rs.s, u, sm);

  _vector_mul_assign(rs.s.c1, coe);
  _vector_mul_assign(rs.s.c2, coe);
  _vector_mul_assign(rs.s.c3, coe);
  _vector_mul_assign(rs.s.c4, coe);
}

/* This is the second stage of the hopping term, this time it distributes the
 * currently active odd point to its nearest neighbours */
static void deo_spatial_dble(int *piup, int *pidn, su3_dble const *u,
                             spinor_dble *pl)
{
  spinor_dble *sp, *sm;

  _vector_mul_assign(rs.s.c1, ceo);
  _vector_mul_assign(rs.s.c2, ceo);
  _vector_mul_assign(rs.s.c3, ceo);
  _vector_mul_assign(rs.s.c4, ceo);

  /**************************** Skip direction 0 ******************************/

  piup += 1;
  pidn += 1;
  u += 1;

  /****************************** direction +1 ********************************/

  sp = pl + (*(piup++));
  u += 1;

  su3_inverse_multiply_add(sp, u, &rs.s);

  /****************************** direction -1 ********************************/

  sm = pl + (*(pidn++));
  u += 1;

  su3_multiply_add(sm, u, &rs.s);

  /****************************** direction +2 ********************************/

  sp = pl + (*(piup++));
  u += 1;

  su3_inverse_multiply_add(sp, u, &rs.s);

  /****************************** direction -2 ********************************/

  sm = pl + (*(pidn++));
  u += 1;

  su3_multiply_add(sm, u, &rs.s);

  /****************************** direction +3 ********************************/

  sp = pl + (*(piup));
  u += 1;

  su3_inverse_multiply_add(sp, u, &rs.s);

  /****************************** direction -3 ********************************/

  sm = pl + (*(pidn));
  u += 1;

  su3_multiply_add(sm, u, &rs.s);
}

void klein_gordon_spatial(double mass_sqr, spinor_dble *psi, spinor_dble *eta)
{
  int *piup, *pidn;

  su3_dble *u, *um;
  spin_t *psio, *etao;

  fetch_full_boundary_spins(psi);
  mulr_spinor_assign_dble(VOLUME, eta, psi, 6. + mass_sqr);
  set_sd2zero(BNDRY / 2, eta + VOLUME);

  coe = -1.0;
  ceo = -1.0;

  piup = iup[VOLUME / 2];
  pidn = idn[VOLUME / 2];

  psio = (spin_t *)(psi + (VOLUME / 2));
  etao = (spin_t *)(eta + (VOLUME / 2));
  u = udfld();
  um = u + 4 * VOLUME;

  for (; u < um; u += 8, piup += 4, pidn += 4, ++psio, ++etao) {
    doe_spatial_dble(piup, pidn, u, psi);

    _vector_add_assign((*etao).s.c1, rs.s.c1);
    _vector_add_assign((*etao).s.c2, rs.s.c2);
    _vector_add_assign((*etao).s.c3, rs.s.c3);
    _vector_add_assign((*etao).s.c4, rs.s.c4);
    rs = (*psio);

    deo_spatial_dble(piup, pidn, u, eta);
  }

  send_full_boundary_spins(eta);
}
