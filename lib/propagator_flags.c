/*******************************************************************************
 *
 * File propagator_flags.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * The externally accessible functions are
 *
 *   dirac_operator_parms_t dirac_operator_parms(void)
 *     Returns the current diract operator parameters.
 *
 *   dirac_operator_parms_t set_dirac_operator_parms(double csw, double mu,
 *                                                   int nkappa, double *kappa,
 *                                                   int smear)
 *     Sets the parameters for the dirac operator, these are:
 *
 *       csw(double)             Coefficient of the Sheikholeslami-Wohlert term.
 *
 *       mu (double)             The twisted mass coefficient.
 *
 *       nkappa (int)            The number of hopping parameter values.
 *
 *       kappa (array(double))   Array of hopping parameter values.
 *
 *       smear (boolean)         Use stout smearing for the operator.
 *
 *******************************************************************************/

#define PROPAGATOR_FLAGS_C
#define OPENQCD_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif

#include "propagator.h"

#include "openqcd/c_headers/flags.h"
#include "openqcd/c_headers/utils.h"

#ifdef __cplusplus
}
#endif

#include <mpi.h>

static dirac_operator_parms_t dop = {0.0, 0};

/* Flag: whether the parameters have been initialised or not */
static int dop_set = 0;

dirac_operator_parms_t dirac_operator_parms(void)
{
  return dop;
}

dirac_operator_parms_t set_dirac_operator_parms(double csw, double mu,
                                                int nkappa, double *kappa,
                                                int smear)
{
  int ie;
  double int_parm_array[1];
  double dbl_parm_array[1];

  error(dop_set != 0, 1, "set_dirac_operator_parms [propagator_flags.c]",
        "Attempt to reset the dirac operator parameters");

  dbl_parm_array[0] = mu;
  int_parm_array[0] = (smear != 0);

  if (NPROC > 1) {
    MPI_Bcast(dbl_parm_array, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
    MPI_Bcast(int_parm_array, 1, MPI_INT, 0, MPI_COMM_WORLD);

    ie = 0;
    ie |= (dbl_parm_array[0] != mu);
    ie |= (int_parm_array[0] != (smear != 0));

    error(ie != 0, 1, "set_dirac_operator_parms [propagator_flags.c]",
          "Parameters not global");
  }

  dop.mu = dbl_parm_array[0];
  dop.smear = int_parm_array[0];

  set_lat_parms(0.0, 1.0, nkappa, kappa, csw);
  set_sw_parms(sea_quark_mass(0));

  dop_set = 1;

  return dop;
}
