/*******************************************************************************
 *
 * File propagator_field.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * The externally accessible functions are
 *
 *   spinor_dble **propagator_field(void)
 *     Access the global propagator storage, memory will be allocated the first
 *     time this function is called.
 *
 * Notes:
 *
 *   The propagator field is laid out in memory as 12 consecutive spinor fields
 *   with and 1 halo at the very end. This means that one might have to be
 *   careful when doing computations on this as accessing the components out of
 *   order might mean that you overwrite data later in the array.
 *
 *   The layout is chosen so that we can work with the Dirac operator in the
 *   naice way and do not have to transform the memory to apply this operator.
 *
 *******************************************************************************/

#define PROPAGATOR_FIELD_C
#define OPENQCD_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif

#include "propagator.h"

#include "openqcd/c_headers/utils.h"

#ifdef __cplusplus
}
#endif

static spinor_dble **pfield = NULL;

/* Allocate memory for the propagator field */
static void allocate_propagator_field(void)
{
  int i;

  error(iup[0][0] == 0, 1, "allocate_propagator_field [propagator_field.c]",
        "Geometry arrays are not set");

  pfield = (spinor_dble **)malloc(12 * sizeof(*pfield));

  error(pfield == NULL, 1, "allocate_propagator_field [propagator_field.c]",
        "Unable to allocate propagator field pointer array");

  /* We allocate 12 spinor fields as well as 1 set of spin boundaries, this will
   * make it so that the i'th propagator component spinor field will use the
   * (i+1)'th propagator as a buffer, but as they are computed sequentially,
   * this doesn't matter. Only the final propagator has its own boundary buffer.
   */
  pfield[0] = (spinor_dble *)amalloc(
      (12 * VOLUME + BNDRY / 2) * sizeof(**pfield), ALIGN);

  error(pfield[0] == NULL, 1, "allocate_propagator_field [propagator_field.c]",
        "Unable to allocate memory space for the propagator fields");

  for (i = 1; i < 12; i++) {
    pfield[i] = pfield[i - 1] + VOLUME;
  }
}

spinor_dble **propagator_field(void)
{
  if (pfield == NULL) {
    allocate_propagator_field();
  }

  return pfield;
}
