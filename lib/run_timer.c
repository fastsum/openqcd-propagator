/*******************************************************************************
 *
 * File run_timer.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * The externally accessible functions are
 *
 *   double start_timer(void)
 *     Start a new timer, return the current time.
 *
 *   void stop_timer(timing_info_t *info, double start_time)
 *     Stop a timer and add the information about the elapsed time since the
 *     start of the timer to a timing_info_t object.
 *   
 *   void add_time(timing_info_t *info, double additional_time)
 *     Add any time value to a timing_info_t object.
 *   
 *   double avg_time(timing_info_t const *info)
 *     Get the average runtimes of all operations registered to that
 *     timing_info_t object.
 *
 * Notes:
 *
 *   The timing_info_t objects are simple structs that accumulates runtimes and
 *   keeps track of the number of times it has been called. It makes extracting
 *   information such as the average time of an operation sometwhat easier.
 *
 *******************************************************************************/

#define RUN_TIMER_C
#define OPENQCD_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif

#include "run_timer.h"

#ifdef __cplusplus
}
#endif
#include <mpi.h>

double start_timer(void)
{
  MPI_Barrier(MPI_COMM_WORLD);
  return MPI_Wtime();
}

void stop_timer(timing_info_t *info, double start_time)
{
  add_time(info, start_timer() - start_time);
}

void add_time(timing_info_t *info, double additional_time)
{
  info->latest = additional_time;
  info->total_time += additional_time;
  info->nops += 1;
}

double avg_time(timing_info_t const *info)
{
  return info->total_time / info->nops;
}
