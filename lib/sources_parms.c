/*******************************************************************************
 *
 * File sources_parms.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * The externally accessible functions are
 *
 *   void reset_source_parms(void)
 *     Resets the source parameters to their initial state. Necessary if you
 *     want to start over (such as in unit tests).
 *   
 *   void set_source_parms(int isrc, source_t type, int pos[4], int random,
 *                         int smear, int num_appl, double smear_parm)
 *     Sets the source parameters for the source with id isrc. These parameters
 *     are:
 *       type (source_t)        Type of the source (point, smeared, ...)
 *
 *       pos (array(int))       Position of the source in global coordinates.
 *
 *       random (boolean)       Whether the position is of a "random" nature,
 *                              see the distribute_random_sources function.
 *
 *       smear (boolean)        Whether or not to apply stout smearing to the
 *                              gauge field before application of e.g. the KG
 *                              operator
 *
 *       num_appl (int)         Number of smearing applications to use in the
 *                              case that source cmearing is used.
 *
 *       smear_parm (double)    Parameter to pass to the smearing routine in the
 *                              case that a source smearing is used.
 *   
 *   void read_source_parms(int isrc)
 *     Read the smearing parameters from the file currently open as stdio and
 *     set them using the above function.
 *   
 *   void print_source_parms(void)
 *     Print all currently set source parameters in a human readable format.
 *   
 *   source_parms_t source_parms(int isrc)
 *     Get the source parameter at index isrc.
 *   
 *   int next_source_id(int isrc)
 *     Given the index isrc, return the next valid source id. If isrc is a valid
 *     source, return isrc. If there are no more sources, return -1.
 *   
 *   char is_random_source(int isrc)
 *     Checks whether the source stored at index isrc is tagged as being at a
 *     random position.
 *   
 *   void distribute_random_sources(source_distribution_t dist, int update_rng)
 *     Distributes the points that have been marked as random. They can
 *     currently be distributed in one of two ways:
 *
 *       RANDOM_DISTRIBUTION          Actually random, every point is placed at
 *                                    a random global lattice point.
 *       
 *       MAX_DISTANCE_DISTRIBUTION    The first point is placed randomly, the
 *                                    remaining points are placed on the corners
 *                                    of the hypercube of half the lattice size
 *                                    in which the first random point is one
 *                                    corner. This works for a maximum of 16
 *                                    sources.
 *
 *******************************************************************************/

#define SOURCES_PARMS_C
#define OPENQCD_INTERNAL

#include "mpi.h"
#include <stdio.h>
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif

#include "sources.h"

#include "openqcd/c_headers/global.h"
#include "openqcd/c_headers/random.h"
#include "openqcd/c_headers/utils.h"

#ifdef __cplusplus
}
#endif

#define N0 (NPROC0 * L0)
#define N1 (NPROC1 * L1)
#define N2 (NPROC2 * L2)
#define N3 (NPROC3 * L3)

static int init = 0, corners_placed = 0, corner[4];
static int *rlxs_state = NULL;
static source_t sources[] = {POINT_SOURCE, GAUSSIAN_SMEARED_SOURCE};
static source_parms_t sp[MAX_SOURCES + 1] = {
    {SOURCES, {0, 0, 0, 0}, 0, 0, 0.0}};
static int has_random_pos[MAX_SOURCES] = {0};

/* Corners of a 4d hypercube to place sources */
static int corner_order[][4] = {
    {1, 1, 1, 1}, {0, 0, 1, 1}, {1, 1, 0, 0}, {0, 1, 0, 1}, {1, 0, 1, 0},
    {0, 1, 1, 0}, {1, 0, 0, 1}, {0, 0, 0, 1}, {1, 1, 1, 0}, {0, 0, 1, 0},
    {1, 1, 0, 1}, {0, 1, 0, 0}, {1, 0, 1, 1}, {1, 0, 0, 0}, {0, 1, 1, 1}};

static void init_sp(void)
{
  int i;

  for (i = 1; i <= MAX_SOURCES; i++) {
    sp[i] = sp[0];
  }

  init = 1;
}

static void save_ranlux(void)
{
  int nlxs;

  if (rlxs_state == NULL) {
    nlxs = rlxs_size();

    rlxs_state = (int *)malloc((nlxs) * sizeof(int));

    error(rlxs_state == NULL, 1, "save_ranlux [sources_random.c]",
          "Unable to allocate state arrays");
  }

  rlxs_get(rlxs_state);
}

static void restore_ranlux(void)
{
  rlxs_reset(rlxs_state);
}

/* Convert a real number between [0, 1] to an integer between [0, n-1] */
static int uniform_to_integer(float f, int n)
{
  return (int)(f * n) % n;
}

static void random_position(int pos[4])
{
  float r[4];

  ranlxs(r, 4);
  pos[0] = uniform_to_integer(r[0], N0);
  pos[1] = uniform_to_integer(r[1], N1);
  pos[2] = uniform_to_integer(r[2], N2);
  pos[3] = uniform_to_integer(r[3], N3);
}

static void next_corner(int pos[4])
{
  int i;

  error_root(corners_placed >= 16, 1, "next_corner [sources_parms.c]",
             "Currently no more than 16 random sources can be placed");

  /* Randomise the first corner */
  if (corners_placed == 0) {
    random_position(pos);

    /* Remember where the corner was places */
    for (i = 0; i < 4; ++i) {
      corner[i] = pos[i];
    }
  } else {
    pos[0] = (corner[0] + (N0 / 2) * corner_order[corners_placed - 1][0]) % N0;
    pos[1] = (corner[1] + (N1 / 2) * corner_order[corners_placed - 1][1]) % N1;
    pos[2] = (corner[2] + (N2 / 2) * corner_order[corners_placed - 1][2]) % N2;
    pos[3] = (corner[3] + (N3 / 2) * corner_order[corners_placed - 1][3]) % N3;
  }

  ++corners_placed;
}

void reset_source_parms(void)
{
  int i;

  if (init == 0) {
    init_sp();
    return;
  }

  for (i = 0; i < MAX_SOURCES; i++) {
    sp[i] = sp[MAX_SOURCES];
  }
}

void set_source_parms(int isrc, source_t type, int pos[4], int random,
                      int smear, int num_appl, double smear_parm)
{
  int i, ie, iprms[8], smear_field, num_application;
  double dprms[1], smearing_parameter;

  if (init == 0) {
    init_sp();
  }

  if (type == POINT_SOURCE || type == SOURCES) {
    smear_field = 0;
    num_application = 0;
    smearing_parameter = 0.0;
  } else if (type == GAUSSIAN_SMEARED_SOURCE) {
    smear_field = (smear != 0);
    num_application = num_appl;
    smearing_parameter = smear_parm;
  }

  if (NPROC > 1) {
    for (i = 0; i < 4; ++i) {
      iprms[i] = pos[i];
    }

    iprms[4] = (random != 0);
    iprms[5] = (int)(type);
    iprms[6] = smear_field;
    iprms[7] = num_application;

    dprms[0] = smearing_parameter;

    MPI_Bcast(iprms, 8, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(dprms, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);

    ie = 0;

    for (i = 0; i < 4; ++i) {
      ie |= (iprms[i] != pos[i]);
    }

    ie |= (iprms[4] != (random != 0));
    ie |= (iprms[5] != (int)(type));
    ie |= (iprms[6] != smear_field);
    ie |= (iprms[7] != num_application);
    ie |= (dprms[0] != smearing_parameter);

    error(ie != 0, 1, "set_source_parms [sources_parms.c]",
          "Parameters not global");
  }

  ie = 0;
  ie |= ((isrc < 0) || (isrc >= MAX_SOURCES));
  ie |= (type == SOURCES);
  ie |= ((pos[0] < 0) || (pos[0] >= NPROC0 * L0));
  ie |= ((pos[1] < 0) || (pos[1] >= NPROC1 * L1));
  ie |= ((pos[2] < 0) || (pos[2] >= NPROC2 * L2));
  ie |= ((pos[3] < 0) || (pos[3] >= NPROC3 * L3));
  ie |= (num_application < 0);

  error_root(ie != 0, 1, "set_source_parms [sources_parms.c]",
             "Invalid source parameters");

  error_root(sp[isrc].type != SOURCES, 1, "set_source_parms [sources_parms.c]",
             "Attempt to reset already specified source parameter");

  sp[isrc].type = type;

  for (i = 0; i < 4; ++i) {
    sp[isrc].pos[i] = pos[i];
  }

  has_random_pos[isrc] = (random != 0);

  sp[isrc].smear_field = smear_field;
  sp[isrc].num_application = num_application;
  sp[isrc].smear_parameter = smearing_parameter;
}

void read_source_parms(int isrc)
{
  int my_rank, i, ids;
  int pos[4], smear, appl, is_random;
  double sparm;
  char line[NAME_SIZE];

  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

  ids = 0;

  for (i = 0; i < 4; ++i) {
    pos[i] = 0;
  }

  is_random = 0;
  smear = 0;
  appl = 0;
  sparm = 0.0;

  if (my_rank == 0) {
    sprintf(line, "Source %d", isrc);
    find_section(line);

    read_line("source", "%s", line);

    if (strcmp(line, "GAUSSIAN_SMEARED_SOURCE") == 0) {
      ids = 1;
      read_line("smear_gauge", "%d", &smear);
      read_line("num", "%d", &appl);
      read_line("kappa", "%lf", &sparm);
    } else if (strcmp(line, "POINT_SOURCE") != 0) {
      error_root(1, 1, "read_source_parm [sources_parms.c]",
                 "Unknown source %s", line);
    }

    read_line("point", "%s", line);

    if (strcmp(line, "RANDOM") == 0) {
      is_random = 1;
    } else {
      read_iprms("point", 4, pos);
    }
  }

  if (NPROC > 1) {
    MPI_Bcast(&ids, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(pos, 4, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&is_random, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&smear, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&appl, 1, MPI_INT, 0, MPI_COMM_WORLD);
    MPI_Bcast(&sparm, 1, MPI_DOUBLE, 0, MPI_COMM_WORLD);
  }

  set_source_parms(isrc, sources[ids], pos, is_random, smear, appl, sparm);
}

void print_source_parms(void)
{
  int my_rank, i, n;

  if (init == 0) {
    return;
  }

  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

  if (my_rank == 0) {
    for (i = 0; i < MAX_SOURCES; ++i) {
      if (sp[i].type == SOURCES) {
        continue;
      }

      printf("Source %d:\n", i);

      switch (sp[i].type) {
      case POINT_SOURCE:
        printf("POINT_SOURCE\n");
        if (has_random_pos[i] == 0) {
          printf("point = [%d, %d, %d, %d]\n\n", sp[i].pos[0], sp[i].pos[1],
                 sp[i].pos[2], sp[i].pos[3]);
        } else {
          printf("point = RANDOM\n");
        }
        break;
      case GAUSSIAN_SMEARED_SOURCE:
        printf("GAUSSIAN_SMEARED_SOURCE\n");
        if (has_random_pos[i] == 0) {
          printf("point = [%d, %d, %d, %d]\n\n", sp[i].pos[0], sp[i].pos[1],
                 sp[i].pos[2], sp[i].pos[3]);
        } else {
          printf("point = RANDOM\n");
        }
        printf("N = %d\n", sp[i].num_application);
        n = fdigits(sp[i].smear_parameter);
        printf("kappa = %.*f\n", IMAX(n, 1), sp[i].smear_parameter);

        if (sp[i].smear_field == 0) {
          printf("Using thin gauge links\n\n");
        } else {
          printf("Using smeared gauge links\n\n");
        }
        break;
      default:
        break;
      }
    }
  }
}

source_parms_t source_parms(int isrc)
{
  if (init == 0) {
    init_sp();
  }

  if ((isrc >= 0) && (isrc < MAX_SOURCES)) {
    return sp[isrc];
  } else {
    error_loc(1, 1, "source_parms [source_parms.c]",
              "Source index is out of range");
    return sp[MAX_SOURCES];
  }
}

int next_source_id(int isrc)
{
  if (init == 0) {
    init_sp();
    return -1;
  }

  if ((isrc < 0) || (isrc >= MAX_SOURCES)) {
    error_loc(1, 1, "next_source_parms [source_parms.c]",
              "Source index is out of range");
    return -1;
  }

  while (isrc < MAX_SOURCES) {
    if (sp[isrc].type != SOURCES) {
      return isrc;
    }
    ++isrc;
  }

  return -1;
}

char is_random_source(int isrc)
{
  error_loc(isrc < 0 || isrc >= MAX_SOURCES, 1,
            "is_random_source [sources_parms.c]", "Source index out of range");
  return (has_random_pos[isrc] != 0);
}

void distribute_random_sources(source_distribution_t dist, int update_rng)
{
  int i, isrc, my_rank;
  int buffer_pos[4];

  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

  if (update_rng == 0) {
    save_ranlux();
  }

  corners_placed = 0;

  for (isrc = 0; isrc < MAX_SOURCES; ++isrc) {
    if (sp[isrc].type == SOURCES || has_random_pos[isrc] == 0) {
      continue;
    }

    if (my_rank == 0) {
      if (dist == RANDOM_DISTRIBUTION) {
        random_position(buffer_pos);
      } else if (dist == MAX_DISTANCE_DISTRIBUTION) {
        next_corner(buffer_pos);
      } else {
        error_root(1, 1, "initiate_random_sources [sources_parms.c]",
                   "Unknown source distribution");
      }
    }

    if (NPROC > 1) {
      MPI_Bcast(buffer_pos, 4, MPI_INT, 0, MPI_COMM_WORLD);
    }

    for (i = 0; i < 4; ++i) {
      sp[isrc].pos[i] = buffer_pos[i];
    }
  }

  if (update_rng == 0) {
    restore_ranlux();
  }
}
