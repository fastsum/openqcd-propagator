/*******************************************************************************
 *
 * File propagator_archive.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * The externally accessible functions are
 *
 *   void export_propagator(char const *filename)
 *     Exports the propagator stored in propagator_field() to a file.
 *
 *   void import_propagator(char const *filename)
 *     Imports a full propagator (12 spinor fields) stored in the file and
 *     stores it in the global propagator_field().
 *
 * Notes:
 *
 *   All functions uses parallelisation and must be called on all processors.
 *
 *   The file format is such that we store the 12x12 components of the
 *   propagator at every lattice site, different to how they are actually laid
 *   out in memory (see propagator_field.c documentation). The files also have
 *   the global lattice size as well as the norm of the propagator prepended as
 *   metadata for checking later.
 *
 *******************************************************************************/

#define PROPAGATOR_ARCHIVE_C
#define OPENQCD_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif

#include "propagator.h"

#include "openqcd/c_headers/lattice.h"
#include "openqcd/c_headers/linalg.h"
#include "openqcd/c_headers/utils.h"

#ifdef __cplusplus
}
#endif

#include <mpi.h>
#include <stdio.h>

#define N0 (NPROC0 * L0)
#define N1 (NPROC1 * L1)
#define N2 (NPROC2 * L2)
#define N3 (NPROC3 * L3)

static int endian;
static spinor_dble **propagator = NULL;
static spinor_dble *pbuf = NULL;

/* Various sanity checks to make sure that the machine has packed its memory
 * correctly */
static void check_machine(void)
{
  error_root(sizeof(stdint_t) != 4, 1, "check_machine [propagator_archive.c]",
             "Size of a stdint_t integer is not 4");
  error_root(sizeof(double) != 8, 1, "check_machine [propagator_archive.c]",
             "Size of a double is not 8");
  error_root(sizeof(spinor_dble) != 192, 1,
             "check_machine [propagator_archive.c]",
             "The spinor_dble structures are not properly packed");

  endian = endianness();
  error_root(endian == openqcd_utils__UNKNOWN_ENDIAN, 1,
             "check_machine [propagator_archive.c]", "Unkown endianness");
}

/* Allocate the send-receive buffer */
static void alloc_pbuf(void)
{
  error(iup[0][0] == 0, 1, "alloc_sbuf [propagator_archive.c]",
        "Geometry arrays are not set");

  pbuf = (spinor_dble *)amalloc(12 * L3 * sizeof(spinor_dble), ALIGN);

  error(pbuf == NULL, 1, "alloc_sbuf [propagator_archive.c]",
        "Unable to allocate auxiliary array");
}

/* Store the value of the global propagator in the send-receive buffer */
static void get_spinor_matrix(int iy)
{
  int idx, y3, iz;
  spinor_dble *pb;

  pb = pbuf;
  iy *= L3;

  for (y3 = 0; y3 < L3; y3++) {
    iz = ipt[iy + y3];
    for (idx = 0; idx < 12; ++idx, ++pb) {
      (*pb) = propagator[idx][iz];
    }
  }
}

/* Store the value of the send-receive buffer in the global propagator */
static void set_spinor_matrix(int iy)
{
  int idx, y3, iz;
  spinor_dble *pb;

  pb = pbuf;
  iy *= L3;

  for (y3 = 0; y3 < L3; y3++) {
    iz = ipt[iy + y3];
    for (idx = 0; idx < 12; ++idx, ++pb) {
      propagator[idx][iz] = (*pb);
    }
  }
}

void export_propagator(char const *filename)
{
  int my_rank, np[4], n, iw;
  int iwa, dmy, tag0, tag1;
  int x0, x1, x2, x3, y0, y1, y2, ix, iy;
  stdint_t lsize[4];
  double norm;
  MPI_Status stat;
  FILE *fout = NULL;

  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

  if (pbuf == NULL) {
    check_machine();
    alloc_pbuf();
  }

  propagator = propagator_field();

  dmy = 1;
  tag0 = mpi_tag();
  tag1 = mpi_tag();

  norm = 0.0;
  for (ix = 0; ix < 12; ++ix) {
    norm += norm_square_dble(VOLUME, 1, propagator[ix]);
  }

  if (my_rank == 0) {
    fout = fopen(filename, "wb");
    error_root(fout == NULL, 1, "export_propagator [propagator_archive.c]",
               "Unable to open output file");

    lsize[0] = N0;
    lsize[1] = N1;
    lsize[2] = N2;
    lsize[3] = N3;

    if (endian == openqcd_utils__BIG_ENDIAN) {
      bswap_int(4, lsize);
      bswap_double(1, &norm);
    }

    iw = fwrite(lsize, sizeof(stdint_t), 4, fout);
    iw += fwrite(&norm, sizeof(double), 1, fout);
    error_root(iw != 5, 1, "export_sfld [propagator_archive.c]",
               "Incorrect write count");
  }

  iwa = 0;

  for (ix = 0; ix < (N0 * N1 * N2); ix++) {
    x0 = ix / (N1 * N2);
    x1 = (ix / N2) % N1;
    x2 = ix % N2;

    y0 = x0 % L0;
    y1 = x1 % L1;
    y2 = x2 % L2;
    iy = y2 + L2 * y1 + L1 * L2 * y0;

    np[0] = x0 / L0;
    np[1] = x1 / L1;
    np[2] = x2 / L2;

    for (x3 = 0; x3 < N3; x3 += L3) {
      np[3] = x3 / L3;
      n = ipr_global(np);

      if (my_rank == n) {
        get_spinor_matrix(iy);
      }

      if (n > 0) {
        /* What is up with the dummy send-recieve? */
        if (my_rank == 0) {
          MPI_Send(&dmy, 1, MPI_INT, n, tag0, MPI_COMM_WORLD);
          MPI_Recv(pbuf, L3 * 288, MPI_DOUBLE, n, tag1, MPI_COMM_WORLD, &stat);
        } else if (my_rank == n) {
          MPI_Recv(&dmy, 1, MPI_INT, 0, tag0, MPI_COMM_WORLD, &stat);
          MPI_Send(pbuf, L3 * 288, MPI_DOUBLE, 0, tag1, MPI_COMM_WORLD);
        }
      }

      if (my_rank == 0) {
        if (endian == openqcd_utils__BIG_ENDIAN) {
          bswap_double(L3 * 288, (double *)(pbuf));
        }

        iw = fwrite(pbuf, 12 * sizeof(spinor_dble), L3, fout);
        iwa |= (iw != L3);
      }
    }
  }

  if (my_rank == 0) {
    error_root(iwa != 0, 1, "export_sfld [propagator_archive.c]",
               "Incorrect write count");
    fclose(fout);
  }
}

void import_propagator(char const *filename)
{
  int my_rank, np[4], n, ir;
  int ira, dmy, tag0, tag1;
  int x0, x1, x2, x3, y0, y1, y2, ix, iy;
  stdint_t lsize[4];
  double norm0, norm1, eps;
  MPI_Status stat;
  FILE *fin = NULL;

  MPI_Comm_rank(MPI_COMM_WORLD, &my_rank);

  if (pbuf == NULL) {
    check_machine();
    alloc_pbuf();
  }

  propagator = propagator_field();

  dmy = 1;
  tag0 = mpi_tag();
  tag1 = mpi_tag();

  if (my_rank == 0) {
    fin = fopen(filename, "rb");
    error_root(fin == NULL, 1, "import_propagator [propagator_archive.c]",
               "Unable to open input file");

    ir = fread(lsize, sizeof(stdint_t), 4, fin);
    ir += fread(&norm0, sizeof(double), 1, fin);
    error_root(ir != 5, 1, "import_propagator [propagator_archive.c]",
               "Incorrect read count");

    if (endian == openqcd_utils__BIG_ENDIAN) {
      bswap_int(4, lsize);
      bswap_double(1, &norm0);
    }

    error_root((lsize[0] != N0) || (lsize[1] != N1) || (lsize[2] != N2) ||
                   (lsize[3] != N3),
               1, "import_propagator [propagator_archive.c]",
               "Lattice sizes do not match");
  } else {
    norm0 = 0.0;
  }

  ira = 0;

  for (ix = 0; ix < (N0 * N1 * N2); ix++) {
    x0 = ix / (N1 * N2);
    x1 = (ix / N2) % N1;
    x2 = ix % N2;

    y0 = x0 % L0;
    y1 = x1 % L1;
    y2 = x2 % L2;
    iy = y2 + L2 * y1 + L1 * L2 * y0;

    np[0] = x0 / L0;
    np[1] = x1 / L1;
    np[2] = x2 / L2;

    for (x3 = 0; x3 < N3; x3 += L3) {
      np[3] = x3 / L3;
      n = ipr_global(np);

      if (my_rank == 0) {
        ir = fread(pbuf, 12 * sizeof(spinor_dble), L3, fin);
        ira |= (ir != L3);

        if (endian == openqcd_utils__BIG_ENDIAN) {
          bswap_double(L3 * 288, (double *)(pbuf));
        }
      }

      if (n > 0) {
        if (my_rank == 0) {
          MPI_Send(pbuf, L3 * 288, MPI_DOUBLE, n, tag1, MPI_COMM_WORLD);
          MPI_Recv(&dmy, 1, MPI_INT, n, tag0, MPI_COMM_WORLD, &stat);
        } else if (my_rank == n) {
          MPI_Recv(pbuf, L3 * 288, MPI_DOUBLE, 0, tag1, MPI_COMM_WORLD, &stat);
          MPI_Send(&dmy, 1, MPI_INT, 0, tag0, MPI_COMM_WORLD);
        }
      }

      if (my_rank == n) {
        set_spinor_matrix(iy);
      }
    }
  }

  if (my_rank == 0) {
    error_root(ira != 0, 1, "import_propagator [propagator_archive.c]",
               "Incorrect read count");
    fclose(fin);
  }

  norm1 = 0.0;
  for (ix = 0; ix < 12; ++ix) {
    norm1 += norm_square_dble(VOLUME, 1, propagator[ix]);
  }
  eps = sqrt(64.0 * (double)(N0 * N1) * (double)(N2 * N3)) * DBL_EPSILON;
  error_root(fabs(norm1 - norm0) > (eps * norm0), 1,
             "import_propagator [propagator_archive.c]",
             "Incorrect square norm");
}
