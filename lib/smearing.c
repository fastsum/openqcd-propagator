/*******************************************************************************
 *
 * File smearing.c
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 * 
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 * 
 * You should have received a copy of the GNU General Public License along with
 * this program. If not, see <https://www.gnu.org/licenses/>.
 *
 * The externally accessible functions are
 *
 *   void gaussian_smearing(spinor_dble *psi, int N, double kappa)
 *     Apply spatial Gaussian smearing to the spinor psi. The smearing operator
 *     is applied N times using kappa as the corresponding smearing hopping
 *     parameter. For more information about this operator see the code
 *     documentation.
 *
 *******************************************************************************/

#define SMEARING_C
#define OPENQCD_INTERNAL

#ifdef __cplusplus
extern "C" {
#endif

#include "smearing.h"
#include "klein_gordon.h"

#include "openqcd/c_headers/global.h"
#include "openqcd/c_headers/linalg.h"
#include "openqcd/c_headers/sflds.h"
#include "openqcd/c_headers/utils.h"

#ifdef __cplusplus
}
#endif

void gaussian_smearing(spinor_dble *psi, int N, double kappa)
{
  int n;
  double effective_mass;
  spinor_dble **ws;

  ws = reserve_wsd(1);
  effective_mass = -(4. * N) / (kappa * kappa);

  for (n = 0; n < N; ++n) {
    mulr_spinor_assign_dble(VOLUME, ws[0], psi, 1. / effective_mass);
    klein_gordon_spatial(effective_mass, ws[0], psi);
  }

  release_wsd();
}
