
/*******************************************************************************
 *
 * File openqcd-propagator.hpp
 *
 * Author (2018): Jonas Rylund Glesaaen
 *
 * This software is distributed under the terms of the GNU General Public
 * License (GPL)
 *
 *******************************************************************************/

#ifndef CPP_OPENQCD__OPENQCD_PROPAGATOR_HPP
#define CPP_OPENQCD__OPENQCD_PROPAGATOR_HPP

#include "openqcd-propagator/klein_gordon.hpp"
#include "openqcd-propagator/propagator.hpp"
#include "openqcd-propagator/run_timer.hpp"
#include "openqcd-propagator/sdcom_extra.hpp"

#endif /* CPP_OPENQCD__OPENQCD_PROPAGATOR_HPP */
