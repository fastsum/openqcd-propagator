
/*
 * Created: 24-09-2018
 * Copyright (c) <2018> <Jonas R. Glesaaen (jonas@glesaaen.com)>
 */

#ifndef CPP_OPENQCD__SOURCES_HPP
#define CPP_OPENQCD__SOURCES_HPP

#include "internal/function_alias.hpp"

extern "C" {
#include "c_headers/sources.h"
}

namespace openqcd {
namespace sources {

using source_t = openqcd_sources__source_t;
using source_distribution_t = openqcd_source__source_distribution_t;
using source_parms_t = openqcd_sources__source_parms_t;

/* SOURCES_C */
OPENQCD_MODULE_FUNCTION_ALIAS(generate_source, sources)

/* SOURCES_PARMS_C */
OPENQCD_MODULE_FUNCTION_ALIAS(reset_source_parms, sources)
OPENQCD_MODULE_FUNCTION_ALIAS(set_source_parms, sources)
OPENQCD_MODULE_FUNCTION_ALIAS(read_source_parms, sources)
OPENQCD_MODULE_FUNCTION_ALIAS(print_source_parms, sources)
OPENQCD_MODULE_FUNCTION_ALIAS(source_parms, sources)
OPENQCD_MODULE_FUNCTION_ALIAS(next_source_id, sources)
OPENQCD_MODULE_FUNCTION_ALIAS(is_random_source, sources)
OPENQCD_MODULE_FUNCTION_ALIAS(distribute_random_sources, sources)

} // namespace sources
} // namespace openqcd

#endif /* CPP_OPENQCD__SOURCES_HPP */
