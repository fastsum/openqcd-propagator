
/*
 * Created: 24-09-2018
 * Copyright (c) <2018> <Jonas R. Glesaaen (jonas@glesaaen.com)>
 */

#ifndef CPP_OPENQCD__SMEARING_HPP
#define CPP_OPENQCD__SMEARING_HPP

#include "internal/function_alias.hpp"

extern "C" {
#include "c_headers/smearing.h"
}

namespace openqcd {
namespace smearing {

/* SMEARING_C */
OPENQCD_MODULE_FUNCTION_ALIAS(gaussian_smearing, propagator)

} // namespace smearing
} // namespace openqcd


#endif /* CPP_OPENQCD__SMEARING_HPP */
