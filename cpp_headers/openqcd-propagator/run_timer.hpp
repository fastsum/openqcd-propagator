
/*******************************************************************************
 *
 * File run_timer.h
 *
 * Author (2018): Jonas Rylund Glesaaen
 *
 * This software is distributed under the terms of the GNU General Public
 * License (GPL)
 *
 *******************************************************************************/

#ifndef CPP_OPENQCD__RUN_TIMER_H
#define CPP_OPENQCD__RUN_TIMER_H

#include "internal/function_alias.hpp"

extern "C"{
#include "c_headers/run_timer.h"
}

namespace openqcd {
namespace run_timer {

using timing_info_t = openqcd_run_timer__timing_info_t;

/* RUN_TIMER_C */
OPENQCD_MODULE_FUNCTION_ALIAS(start_timer, run_timer)
OPENQCD_MODULE_FUNCTION_ALIAS(stop_timer, run_timer)
OPENQCD_MODULE_FUNCTION_ALIAS(add_time, run_timer)
OPENQCD_MODULE_FUNCTION_ALIAS(avg_time, run_timer)

} // namespace run_timer
} // namespace openqcd

#endif /* CPP_OPENQCD__RUN_TIMER_H */
