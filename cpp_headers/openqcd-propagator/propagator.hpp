
/*******************************************************************************
 *
 * File propagator.hpp
 *
 * Author (2018): Jonas Rylund Glesaaen
 *
 * This software is distributed under the terms of the GNU General Public
 * License (GPL)
 *
 *******************************************************************************/

#ifndef CPP_OPENQCD__PROPAGATOR_HPP
#define CPP_OPENQCD__PROPAGATOR_HPP

#include "internal/function_alias.hpp"

extern "C" {
#include "c_headers/propagator.h"
}

namespace openqcd {
namespace propagator {

using dirac_operator_parms_t = openqcd_propagator__dirac_operator_parms_t;

/* PROPAGATOR_FLAGS_C */
OPENQCD_MODULE_FUNCTION_ALIAS(dirac_operator_parms, propagator)
OPENQCD_MODULE_FUNCTION_ALIAS(set_dirac_operator_parms, propagator)

/* PROPAGATOR_FIELD_C */
OPENQCD_MODULE_FUNCTION_ALIAS(propagator_field, propagator)

/* PROPAGATOR_ARCHIVE_C */
OPENQCD_MODULE_FUNCTION_ALIAS(export_propagator, propagator)
OPENQCD_MODULE_FUNCTION_ALIAS(import_propagator, propagator)

/* PROPAGATOR_C */
OPENQCD_MODULE_FUNCTION_ALIAS(solve_dirac, propagator)
OPENQCD_MODULE_FUNCTION_ALIAS(compute_propagator, propagator)

} // namespace propagator
} // namespace openqcd

#endif /* CPP_OPENQCD__PROPAGATOR_HPP */
