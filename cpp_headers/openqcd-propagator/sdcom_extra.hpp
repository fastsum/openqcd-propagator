
/*
 * Created: 07-06-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef CPP_OPENQCD__SDCOM_EXTRA_HPP
#define CPP_OPENQCD__SDCOM_EXTRA_HPP

#include "internal/function_alias.hpp"

extern "C" {
#include "c_headers/sdcom_extra.h"
}

namespace openqcd {
namespace sdcom_extra {

// SDCOM_EXTRA_C
OPENQCD_MODULE_FUNCTION_ALIAS(fetch_full_boundary_spins, propagator)
OPENQCD_MODULE_FUNCTION_ALIAS(send_full_boundary_spins, propagator)

} // namespace sdcom_extra
} // namespace openqcd

#endif /* CPP_OPENQCD__SDCOM_EXTRA_HPP */
