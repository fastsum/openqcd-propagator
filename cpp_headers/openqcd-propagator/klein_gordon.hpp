
/*
 * Created: 07-06-2018
 * Author: Jonas R. Glesaaen (jonas@glesaaen.com)
 */

#ifndef CPP_OPENQCD__KLEIN_GORDON_HPP
#define CPP_OPENQCD__KLEIN_GORDON_HPP

#include "internal/function_alias.hpp"

extern "C" {
#include "c_headers/klein_gordon.h"
}

// KLEIN_GORDON_C

namespace openqcd {
namespace klein_gordon {

OPENQCD_MODULE_FUNCTION_ALIAS(klein_gordon_spatial, klein_gordon)

} // namespace klein_gordon
} // namespace openqcd

#endif /* CPP_OPENQCD__KLEIN_GORDON_HPP */
