/*******************************************************************************
 *
 * File sources.h
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#ifndef OPENQCD__SOURCES_H
#define OPENQCD__SOURCES_H

#include "openqcd/c_headers/su3.h"

#define OPENQCD_MU_SQUARED__MAX_SOURCES 32

typedef enum
{
  SOURCES,
  POINT_SOURCE,
  GAUSSIAN_SMEARED_SOURCE
} openqcd_sources__source_t;

typedef enum
{
  RANDOM_DISTRIBUTION,
  MAX_DISTANCE_DISTRIBUTION,
} openqcd_source__source_distribution_t;

typedef struct
{
  openqcd_sources__source_t type;
  int pos[4];
  int smear_field;
  int num_application;
  double smear_parameter;
} openqcd_sources__source_parms_t;

/* SOURCES_C */
extern void openqcd_sources__generate_source(int isrc, int spin_colour_index,
                                             openqcd__spinor_dble *psi);

/* SOURCES_PARMS_C */
extern void openqcd_sources__reset_source_parms(void);
extern void openqcd_sources__set_source_parms(int isrc,
                                              openqcd_sources__source_t type,
                                              int pos[4], int random, int smear,
                                              int num_appl, double smear_parm);
extern void openqcd_sources__read_source_parms(int isrc);
extern void openqcd_sources__print_source_parms(void);
extern openqcd_sources__source_parms_t openqcd_sources__source_parms(int isrc);
extern int openqcd_sources__next_source_id(int isrc);
extern char openqcd_sources__is_random_source(int isrc);
extern void openqcd_sources__distribute_random_sources(
    openqcd_source__source_distribution_t dist, int update_rng);

#if defined(OPENQCD_INTERNAL)
#define MAX_SOURCES OPENQCD_MU_SQUARED__MAX_SOURCES
#define source_t openqcd_sources__source_t
#define source_distribution_t openqcd_source__source_distribution_t
#define source_parms_t openqcd_sources__source_parms_t

/* SOURCES_C */
#define generate_source(...) openqcd_sources__generate_source(__VA_ARGS__)

/* SOURCES_PARMS_C */
#define reset_source_parms(...) openqcd_sources__reset_source_parms(__VA_ARGS__)
#define set_source_parms(...) openqcd_sources__set_source_parms(__VA_ARGS__)
#define read_source_parms(...) openqcd_sources__read_source_parms(__VA_ARGS__)
#define print_source_parms(...) openqcd_sources__print_source_parms(__VA_ARGS__)
#define source_parms(...) openqcd_sources__source_parms(__VA_ARGS__)
#define next_source_id(...) openqcd_sources__next_source_id(__VA_ARGS__)
#define is_random_source(...) openqcd_sources__is_random_source(__VA_ARGS__)
#define distribute_random_sources(...)                                           \
  openqcd_sources__distribute_random_sources(__VA_ARGS__)

#endif /* defined OPENQCD_INTERNAL */

#endif /* OPENQCD__SOURCES_H */
