/*******************************************************************************
 *
 * File run_timer.h
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#ifndef OPENQCD__RUN_TIMER_H
#define OPENQCD__RUN_TIMER_H

typedef struct
{
  double total_time;
  double latest;
  int nops;
} openqcd_run_timer__timing_info_t;

/* RUN_TIMER_C */
extern double openqcd_run_timer__start_timer(void);
extern void
openqcd_run_timer__stop_timer(openqcd_run_timer__timing_info_t *info,
                              double start_time);
extern void openqcd_run_timer__add_time(openqcd_run_timer__timing_info_t *info,
                                        double additional_time);
extern double
openqcd_run_timer__avg_time(openqcd_run_timer__timing_info_t const *info);

#if defined(OPENQCD_INTERNAL)
#define timing_info_t openqcd_run_timer__timing_info_t

/* RUN_TIMER_C */
#define start_timer(...) openqcd_run_timer__start_timer(__VA_ARGS__)
#define stop_timer(...) openqcd_run_timer__stop_timer(__VA_ARGS__)
#define add_time(...) openqcd_run_timer__add_time(__VA_ARGS__)
#define avg_time(...) openqcd_run_timer__avg_time(__VA_ARGS__)

#endif /* defined OPENQCD_INTERNAL */

#endif /* OPENQCD__RUN_TIMER_H */
