/*******************************************************************************
 *
 * File propagator.h
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#ifndef OPENQCD__PROPAGATOR_H
#define OPENQCD__PROPAGATOR_H

#include "openqcd/c_headers/global.h"
#include "run_timer.h"

typedef struct
{
  double mu;
  int smear;
} openqcd_propagator__dirac_operator_parms_t;

/* PROPAGATOR_FLAGS_C */
extern openqcd_propagator__dirac_operator_parms_t
openqcd_propagator__dirac_operator_parms(void);
extern openqcd_propagator__dirac_operator_parms_t
openqcd_propagator__set_dirac_operator_parms(double csw, double mu, int nkappa,
                                             double *kappa, int smear);

/* PROPAGATOR_FIELD_C */
extern openqcd__spinor_dble **openqcd_propagator__propagator_field(void);

/* PROPAGATOR_ARCHIVE_C */
extern void openqcd_propagator__export_propagator(char const *filename);
extern void openqcd_propagator__import_propagator(char const *filename);

/* PROPAGATOR_C */
extern void openqcd_propagator__solve_dirac(openqcd__spinor_dble *source,
                                            openqcd__spinor_dble *target,
                                            int isolv, int *status);
extern openqcd_run_timer__timing_info_t
openqcd_propagator__compute_propagator(int isrc, int *status);

#if defined(OPENQCD_INTERNAL)
#define dirac_operator_parms_t openqcd_propagator__dirac_operator_parms_t

/* PROPAGATOR_FLAGS_C */
#define dirac_operator_parms(...)                                              \
  openqcd_propagator__dirac_operator_parms(__VA_ARGS__)
#define set_dirac_operator_parms(...)                                          \
  openqcd_propagator__set_dirac_operator_parms(__VA_ARGS__)

/* PROPAGATOR_FIELD_C */
#define propagator_field(...) openqcd_propagator__propagator_field(__VA_ARGS__)

/* PROPAGATOR_ARCHIVE_C */
#define export_propagator(...)                                                 \
  openqcd_propagator__export_propagator(__VA_ARGS__)
#define import_propagator(...)                                                 \
  openqcd_propagator__import_propagator(__VA_ARGS__)

/* PROPAGATOR_C */
#define solve_dirac(...) openqcd_propagator__solve_dirac(__VA_ARGS__)
#define compute_propagator(...)                                                \
  openqcd_propagator__compute_propagator(__VA_ARGS__)

#endif /* defined OPENQCD_INTERNAL */

#endif /* OPENQCD__PROPAGATOR_H */
