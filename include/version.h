/*******************************************************************************
 *
 * File include.h
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#ifndef OPENQCD_PROPAGATOR__VERSION_H
#define OPENQCD_PROPAGATOR__VERSION_H

#define openqcd_propagator_RELEASE "openqcd-fastsum v1.0"

extern const char *openqcd_propagator__build_date;
extern const char *openqcd_propagator__build_git_sha;
extern const char *openqcd_propagator__build_user_cflags;

#endif /* OPENQCD_PROPAGATOR__VERSION_H */
