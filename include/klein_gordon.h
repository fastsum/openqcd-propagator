/*******************************************************************************
 *
 * File klein_gordon.h
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#ifndef OPENQCD__KLEIN_GORDON_H
#define OPENQCD__KLEIN_GORDON_H

#include "openqcd/c_headers/su3.h"

/* KLEIN_GORDON_C */
void openqcd_klein_gordon__klein_gordon_spatial(double mass_sqr,
                                                openqcd__spinor_dble *psi,
                                                openqcd__spinor_dble *eta);

#if defined(OPENQCD_INTERNAL)

/* KLEIN_GORDON_C */
#define klein_gordon_spatial(...)                                              \
  openqcd_klein_gordon__klein_gordon_spatial(__VA_ARGS__)

#endif /* defined OPENQCD_INTERNAL */

#endif /* OPENQCD__KLEIN_GORDON_H */
