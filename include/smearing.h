/*******************************************************************************
 *
 * File smearing.h
 *
 * Copyright (C) 2018 Jonas Rylund Glesaaen
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 *******************************************************************************/

#ifndef OPENQCD_PROPAGATOR__SMEARING_H
#define OPENQCD_PROPAGATOR__SMEARING_H

#include "openqcd/c_headers/su3.h"

/* SMEARING_C */
void openqcd_propagator__gaussian_smearing(openqcd__spinor_dble *psi, int N, double kappa);

#if defined(OPENQCD_INTERNAL)

/* SMEARING_C */
#define gaussian_smearing(...) openqcd_propagator__gaussian_smearing(__VA_ARGS__)

#endif /* defined OPENQCD_INTERNAL */

#endif /* OPENQCD_PROPAGATOR__SMEARING_H */
