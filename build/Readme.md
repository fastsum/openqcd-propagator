# Compiling openqcd-propagator

The openqcd-propagator build process is more or less the same as the one of
openqcd-fastsum. Configuration of the build is handled using the
`compile-settings.txt` file, and the build is carried out in a relative manner,
making it easy to keep multiple versions of the executables and libraries.

## Prerequisites

The code depends on openQCD-FASTSUM v1.1 (which includes the library feature) as
it uses openQCD as a back-end. The header and library files produced by openQCD
must be available at compilation, and one can specify its path to the compiler
through setting the `CFLAGS` and `LDFLAGS` of the configuration (see next
section), or by setting the `OPENQCDLOC` flag. The code assumes that
openQCD-FASTSUM has been compiled as a C++ library.

To compile the tests one also has to have a C++14 compatible MPI compiler
available.

## Options

The compilation has the following options:

```
 * CODELOC (required):
     Path to the root directory of the code, can be relative or absolute
 * OPENQCDLOC (optional):
     Root location of the openqcd-fastsum library installation
 * COMPILER.CC (optional, default: mpicc):
     Command to use to compile C-code
 * COMPILER.CXX (optional, default: mpicxx):
     Command to use to compile C++ code, the unit tests
 * MPI_INCLUDE (optional):
     Directory where the MPI header files are located
 * CFLAGS (optional):
     Additional C-flags to compile with
 * LDFLAGS (optional):
     Additional linker flags to compile with
```

An example compile_settings file is included in the repository. 

## Build targets

The makefile have multiple targets depending on what needs to be compiled:

 * `all`: (default) compiles the `openqcd-fastsum` executable
 * `tests`: compiles the unit tests
 * `library`: compiles the library and copies the header files to the build-dir
 * `library-cpp`: compiles the library and copies the C++ headers
 * `install`: compile library and executables and move them to `$PREFIX`
 * `install-cpp`: compile C++ library and executables and move them to `$PREFIX`

In the case of `install` and `install-cpp` everything is compiled, the binaries
are copied to `$PREFIX/bin`, the library files to `$PREFIX/lib` and the headers
to `$PREFIX/include-cpp`. Setting `PREFIX` can for instance be done by calling
`make` as:

```bash
make install-cpp PREFIX=$HOME/usr
```

If `PREFIX` isn't set it defaults to `.`, the current directory.
